# Conference management system #

Master rad: **Primena reaktivnog programiranja i Spring okvira u razvoju Java aplikacija**

Fakultet organizacionih nauka, Univerzitet u Beogradu

### Opis ###
Conference management system je sistem namenjen za upravljenje korisnickim prijavama na stručnim konferencijama.
Pored glavnog usera SUPERADMIN, koji kreira konferencije, korisnici imaju tri vrste uloga:
- admin konferencije - CONFERENCE_ADMIN
- strucni clan - COMMITTEE_MEMBER
- ucesnik konferencije (korisnik) - USER

Za potrebe master rada, fokus je na strucnom clanu i ucesniku konferencije, kao korisnicima koji imaju najvise 
slucajeva koriscenja. Admin konferencije bi imao sve privilegije kao i strucni clan, sa dodatkom kreiranje 
raznih specificnih podataka za konferenciju.

Korisnik se prijavuje/registruje na sistem, nakon cega ima mogucnost da bira konferenciju koju zeli da otvori 
ili pristupa direktnim linkom na stranicu konferencije. Ukoliko korisnik prvi put zeli da poseti stranicu konferencije,
morace da podnese zahtev za registracijom na aplikaciju konferencije, koju odobrava/odbija strucni clan ili admin.
Kada je ucesniku odobrena registracija, on moze podneti prijavu za konferenciju. Prijava se sastoji od strucnog rada,
u vidu dokumenta, kao i pratecih podataka uz prijavu (opis, kategorija,..).
Nakon kreirane prijave, strucni clan uzima aplikaciju na pregledanje. Tokom pregledanja, strucni clan i ucesnik
mogu razmenjivati komentare na stranici aplikacije. A nakon pregledannja strucni clan prihvata ili odbija aplikaciju.
Ukoliko korisnik 2 odobravanja (2 strucna clana su odobrila aplikaciju), on moze potvrditi svoju prijavu,
te postaje ucesnik predstojece konferencije.

### Arhitektura ###
Aplikacija koristi "headless" arhitekturu. To znaci da postoje dva dela aplikacije, Frontend i Backend deo.

BE deo aplikacije implementiran tako da postuje principe reaktivniog programiranje, tako da je ceo sistem reaktivan
ukljucujuci i pristup bazi. Napsan je pomocu Spring frameworka i projekta Reactor.
Kako bi i pristup bazi, kao blokirajuci proces, bio asinhron i reaktivan, koriscen je r2dbc-mysql driver.

![Alt text](src/main/resources/images/Confms_NEW.png?raw=true "Title")

Frontend deo aplikacije implementiran je u Reactjs.
https://bitbucket.org/bskoric/conference-management-system-frontend/src/master/


Za potrebe testiranja, svi postojeci korisnici, koji su vec u bazi podataka, koriste istu lozinku 'Asd12345'.

