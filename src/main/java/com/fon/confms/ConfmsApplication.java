package com.fon.confms;

import com.fon.confms.dao.ApplyCommentDAO;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ConfmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfmsApplication.class, args);
    }

    @Bean
    ApplicationRunner run(ApplyCommentDAO dao) {
        return args -> dao.findAll().subscribe(System.out::println);
    }
}
