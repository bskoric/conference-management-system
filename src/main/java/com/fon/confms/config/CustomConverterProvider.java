package com.fon.confms.config;

import com.fon.confms.dao.converter.AppCommentReadConverter;
import com.fon.confms.dao.converter.AppCommentWriteConverter;
import com.fon.confms.dao.converter.ApplyReadConverter;
import com.fon.confms.dao.converter.ApplyWriteConverter;
import com.fon.confms.dao.converter.ConferenceReadConverter;
import com.fon.confms.dao.converter.ConferenceSectionReadConverter;
import com.fon.confms.dao.converter.ConferenceSectionWriteConverter;
import com.fon.confms.dao.converter.ConferenceWriteConverter;
import com.fon.confms.dao.converter.CountryReadConverter;
import com.fon.confms.dao.converter.DocumentCategoryReadConverter;
import com.fon.confms.dao.converter.DocumentCategoryWriteConverter;
import com.fon.confms.dao.converter.DocumentReadConverter;
import com.fon.confms.dao.converter.DocumentWriteConverter;
import com.fon.confms.dao.converter.ReviewReadConverter;
import com.fon.confms.dao.converter.ReviewWriteConverter;
import com.fon.confms.dao.converter.UserReadConverter;
import com.fon.confms.dao.converter.UserWriteConverter;

import java.util.ArrayList;
import java.util.List;

public final class CustomConverterProvider {

    private CustomConverterProvider() {

    }

    public static List<Object> getConverters() {
        List<Object> converterList = new ArrayList<>();
        converterList.add(new UserReadConverter());
        converterList.add(new UserWriteConverter());
        converterList.add(new ConferenceReadConverter());
        converterList.add(new ConferenceWriteConverter());
        converterList.add(new DocumentReadConverter());
        converterList.add(new DocumentWriteConverter());
        converterList.add(new ApplyReadConverter());
        converterList.add(new ApplyWriteConverter());
        converterList.add(new ReviewReadConverter());
        converterList.add(new ReviewWriteConverter());
        converterList.add(new AppCommentReadConverter());
        converterList.add(new AppCommentWriteConverter());
        converterList.add(new ConferenceSectionReadConverter());
        converterList.add(new ConferenceSectionWriteConverter());
        converterList.add(new DocumentCategoryReadConverter());
        converterList.add(new DocumentCategoryWriteConverter());
        converterList.add(new CountryReadConverter());
        return converterList;
    }
}
