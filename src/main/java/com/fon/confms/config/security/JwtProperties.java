package com.fon.confms.config.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtProperties {
    private String secretKey = "confms_key_rzxlszzxsqcysyhljt";
    private long validityInMs = 18000000; // 5h
}