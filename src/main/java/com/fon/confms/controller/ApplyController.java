package com.fon.confms.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fon.confms.dto.ResponseDTO;
import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.service.ApplyService;
import com.fon.confms.validator.DocumentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/applications")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class ApplyController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DocumentValidator documentValidator;

    @Resource
    private ApplyService applyService;

    @GetMapping("/{applicationId}")
    public Mono<ApplyModel> getApply(@PathVariable int applicationId) {
        return applyService.getByID(applicationId);
    }

    @GetMapping("/{applicationId}/conferences/{conferenceId}")
    public Mono<ApplyModel> getApply(@PathVariable int applicationId, @PathVariable int conferenceId) {
        return applyService.getApply(applicationId, conferenceId);
    }

    @GetMapping("/conferences/{conferenceId}/users/{userId}")
    public Flux<ApplyModel> getApplications(@PathVariable int userId, @PathVariable int conferenceId) {
        return applyService.getByUserAndConference(conferenceId, userId);
    }

    @GetMapping("/conferences/{conferenceId}/committee-member/{committeeMemberId}")
    public Mono<ResponseEntity<ResponseDTO>> getApplicationsForReview(@PathVariable int conferenceId, @PathVariable int committeeMemberId) {
        return applyService.getApplicationsForCommitteeMember(conferenceId, committeeMemberId)
                           .collectList().map(applications -> ResponseEntity.ok(new ResponseDTO(applications, "")))
                           .onErrorResume(error -> Mono.just(ResponseEntity.badRequest()
                                                                           .body(new ResponseDTO(null, error.getMessage()))));
    }

    @GetMapping("/{applicationId}/comments")
    public ResponseEntity<Flux<ApplicationCommentModel>> getCommentsForApplication(@PathVariable int applicationId) {
        return ResponseEntity.ok(applyService.getCommentsForApplication(applicationId));
    }

    @PostMapping("/{applicationId}/comments")
    public Mono<ResponseEntity<ApplicationCommentModel>> addCommentForApplication(@RequestBody ApplicationCommentModel comment) {
        return applyService.createComment(comment).map(ResponseEntity::ok);
    }

    @DeleteMapping("/comments/{commentId}/users/{userId}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable int commentId, @PathVariable int userId) {
        return applyService.deleteComment(commentId, userId).map(ResponseEntity::ok);
    }

    @PostMapping("/{applicationId}/close")
    public Mono<ResponseEntity<Void>> closeApplication(@PathVariable int applicationId) {
        return applyService.closeApplication(applicationId).map(ResponseEntity::ok);
    }

    @DeleteMapping("/{applicationId}/delete")
    public Mono<ResponseEntity<Void>> deleteApplication(@PathVariable int applicationId) {
        return applyService.deleteApplication(applicationId).map(ResponseEntity::ok);
    }

    @PostMapping("/{applicationId}/finish")
    public Mono<ResponseEntity<ResponseDTO>> finishApplication(@PathVariable int applicationId) {
        return applyService.finishApplication(applicationId)
                           .map(finishedApp -> ResponseEntity.ok(new ResponseDTO(finishedApp, "")))
                           .onErrorResume(error -> Mono.just(ResponseEntity.badRequest()
                                                                           .body(new ResponseDTO(null, error.getMessage()))));
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<ResponseEntity<ResponseDTO>> createApply(@RequestPart("document") String jsonDocument,
                                                         @RequestPart(value = "file", required = false) FilePart file) {

        return DataBufferUtils.join(file.content())
                              .map(this::getBytes)
                              .flatMap(bytes -> Mono.fromCallable(() -> objectMapper.readValue(jsonDocument, DocumentModel.class))
                                                    .flatMap(documentModel -> validateDocumentAndCreateApply(documentModel,
                                                                                                             bytes,
                                                                                                             file)));
    }

    private byte[] getBytes(DataBuffer dataBuffer) {
        byte[] bytes = new byte[dataBuffer.readableByteCount()];
        dataBuffer.read(bytes);
        DataBufferUtils.release(dataBuffer);
        return bytes;
    }

    private Mono<ResponseEntity<ResponseDTO>> validateDocumentAndCreateApply(DocumentModel documentModel, byte[] bytes, FilePart file) {
        Errors errors = new BeanPropertyBindingResult(documentModel, "Document");
        documentValidator.validate(documentModel, errors);

        if (errors.hasErrors() && Objects.nonNull(errors.getFieldError())) {
            return Mono.just(ResponseEntity.badRequest().body(new ResponseDTO(null, errors.getFieldError().getDefaultMessage())));
        }

        return applyService.activeApplyNotExist(documentModel).flatMap(applyNotExist -> {
            if (applyNotExist) {
                documentModel.setFile(bytes);
                documentModel.setFileName(file.filename());
                return applyService.createApply(documentModel).map(apply -> ResponseEntity.ok(new ResponseDTO(apply, "")));
            }
            return Mono.just(ResponseEntity.badRequest().body(new ResponseDTO(null, "Apply already exist")));
        });
    }
}
