package com.fon.confms.controller;

import com.fon.confms.dto.UserStatusDTO;
import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.service.UserService;
import com.fon.confms.service.conference.ConferenceCategoryService;
import com.fon.confms.service.conference.ConferenceSectionService;
import com.fon.confms.service.conference.ConferenceService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/conferences")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class ConferenceController {

    @Resource
    private ConferenceService conferenceService;

    @Resource
    private UserService userService;

    @Resource
    private ConferenceCategoryService conferenceCategoryService;

    @Resource
    private ConferenceSectionService conferenceSectionService;

    @GetMapping
    public Flux<ConferenceModel> getAllConferences() {
        return conferenceService.getAll();
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<ConferenceModel>> getConference(@PathVariable int id) {
        return conferenceService.getByID(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    /**
     * Returning participants of the conference
     *
     * @param conferenceId id of conference for which participants are returned
     * @return Flux<UserModel>
     */
    @GetMapping("/{conferenceId}/users")
    public Flux<UserModel> getConferenceMembers(@PathVariable int conferenceId) {
        return conferenceService.getConferenceMembers(conferenceId);
    }

    /**
     * Returning users that are committee members of the conference
     *
     * @param conferenceId id of conference
     * @return Flux<UserModel>
     */
    @GetMapping("/{conferenceId}/committee-members")
    public Flux<UserModel> getCommitteeMembers(@PathVariable int conferenceId) {
        return conferenceService.getCommitteeMembers(conferenceId);
    }

    @GetMapping("/{conferenceId}/users/{userId}/is-member")
    public Mono<UserStatusDTO> getUserConferenceStatus(@PathVariable int conferenceId, @PathVariable int userId) {
        return conferenceService.isUserRegisterToConferenceApp(conferenceId, userId).flatMap(isRegistered -> {
            if (isRegistered) {
                return userService.getUserConferenceStatus(conferenceId, userId).map(status -> new UserStatusDTO(true, status));
            }
            return userService.getUserConferenceStatus(conferenceId, userId).map(status -> new UserStatusDTO(false, status));
        });
    }

    @PostMapping(("/{conferenceId}/users/{userId}/register"))
    @PreAuthorize("hasRole('ROLE_USER')")
    public Mono<Boolean> registerToConference(@PathVariable int conferenceId, @PathVariable int userId) {
        return conferenceService.registerToConferenceApp(conferenceId, userId);
    }

    /**
     * Returning sections of the conference
     *
     * @param conferenceId id of conference
     * @return Flux<ConferenceSectionModel>
     */
    @GetMapping("/{conferenceId}/sections")
    public Flux<ConferenceSectionModel> getSections(@PathVariable int conferenceId) {
        return conferenceSectionService.getSectionsForConference(conferenceId);
    }

    @GetMapping("/categories")
    public Flux<ConferenceCategoryModel> getCategories() {
        return conferenceCategoryService.getAll();
    }

    @GetMapping("/categories/{categoryId}")
    public Mono<ConferenceCategoryModel> getCategory(@PathVariable int categoryId) {
        return conferenceCategoryService.getByID(categoryId);
    }
}
