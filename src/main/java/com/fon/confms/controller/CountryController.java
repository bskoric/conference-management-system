package com.fon.confms.controller;

import com.fon.confms.model.CountryModel;
import com.fon.confms.service.CountryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/countries")
public class CountryController {

    @Resource
    private CountryService countryService;

    @GetMapping
    public Flux<CountryModel> getAll() {
        return countryService.getAll();
    }
}
