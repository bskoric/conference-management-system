package com.fon.confms.controller;

import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.service.DocumentService;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/documents")
public class DocumentController {

    @Resource
    private DocumentService documentService;

    @GetMapping("/{id}")
    public Mono<ResponseEntity<DocumentModel>> getDocument(@PathVariable int id) {
        return documentService.getByID(id).map(ResponseEntity::ok);
    }

    @GetMapping("/{id}/file")
    public Mono<ResponseEntity<byte[]>> getDocumentFileForDownload(@PathVariable int id) {
        return documentService.getByID(id).map(document -> {
            HttpHeaders headers = new HttpHeaders();
            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + document.getFileName());
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
            return ResponseEntity.ok().headers(headers).body(document.getFile());
        });
    }

    @GetMapping("/categories")
    public Flux<DocumentCategoryModel> getDocumentCategories() {
        return documentService.getCategories();
    }

    @GetMapping("/categories/{categoryId}")
    public Mono<DocumentCategoryModel> getDocumentCategory(@PathVariable int categoryId) {
        return documentService.getCategory(categoryId);
    }
}
