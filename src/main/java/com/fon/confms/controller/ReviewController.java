package com.fon.confms.controller;

import com.fon.confms.dto.ResponseDTO;
import com.fon.confms.dto.request.CreateReviewRequestDTO;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.enums.ReviewStatusEnum;
import com.fon.confms.service.ReviewService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/reviews")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class ReviewController {

    @Resource
    private ReviewService reviewService;

    @GetMapping("/conferences/{conferenceId}/users/{userId}")
    public ResponseEntity<Flux<ReviewModel>> getReviews(@PathVariable int userId, @PathVariable int conferenceId) {
        return ResponseEntity.ok(reviewService.getReviews(userId, conferenceId));
    }

    @GetMapping("/applications/{applicationId}")
    public ResponseEntity<Flux<ReviewModel>> getReviews(@PathVariable int applicationId) {
        return ResponseEntity.ok(reviewService.getReviews(applicationId));
    }

    @GetMapping("/conferences/{conferenceId}/committee-member/{committeeMemberId}/apply-status/{status}")
    public ResponseEntity<Flux<ReviewModel>> getReviews(@PathVariable int conferenceId,
                                                        @PathVariable int committeeMemberId,
                                                        @PathVariable String status) {
        return ResponseEntity.ok(reviewService.getReviews(committeeMemberId, conferenceId, status));
    }

    @GetMapping("/{reviewId}")
    public ResponseEntity<Mono<ReviewModel>> getReview(@PathVariable int reviewId) {
        return ResponseEntity.ok(reviewService.getByID(reviewId));
    }

    @GetMapping("/applications/{applicationId}/committee-member/{committeeMemberId}")
    public ResponseEntity<Mono<ReviewModel>> getReview(@PathVariable int applicationId, @PathVariable int committeeMemberId) {
        return ResponseEntity.ok(reviewService.getReview(committeeMemberId, applicationId));
    }

    @PostMapping("/{reviewId}/status")
    @PreAuthorize("hasRole('ROLE_COMMITTEE_MEMBER') or hasRole('CONFERENCE_ADMIN')")
    public Mono<ResponseEntity<Void>> changeReviewStatus(@PathVariable int reviewId, @RequestParam final ReviewStatusEnum type) {
        return reviewService.changeStatus(reviewId, type).map(ResponseEntity::ok);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_COMMITTEE_MEMBER') or hasRole('CONFERENCE_ADMIN')")
    public Mono<ResponseEntity<ResponseDTO>> createReview(@RequestBody CreateReviewRequestDTO review) {
        return reviewService.createReview(review.getApplication(), review.getCommitteeMember())
                            .map(reviewModel -> ResponseEntity.ok(new ResponseDTO(reviewModel, "")))
                            .onErrorResume(error -> Mono.just(ResponseEntity.badRequest().body(new ResponseDTO(null, error.getMessage()))));
    }
}
