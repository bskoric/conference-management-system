package com.fon.confms.controller;

import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.service.conference.ConferenceSectionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/sections")
public class SectionController {

    @Resource
    private ConferenceSectionService conferenceSectionService;

    @GetMapping("/{sectionId}")
    public Mono<ConferenceSectionModel> getSection(@PathVariable int sectionId) {
        return conferenceSectionService.getByID(sectionId);
    }

    @GetMapping("/categories/{categoryId}")
    public Flux<ConferenceSectionModel> getSectionsOfCategory(@PathVariable int categoryId) {
        return conferenceSectionService.getSectionsForCategory(categoryId);
    }

    @GetMapping("/committee-member/{committeeMemberId}")
    public Flux<ConferenceSectionModel> getSectionsOfCommitteeMember(@PathVariable int committeeMemberId) {
        return conferenceSectionService.getSectionsForCommitteeMember(committeeMemberId);
    }
}
