package com.fon.confms.controller.user;

import com.fon.confms.model.UserModel;
import com.fon.confms.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/{userId}")
    public Mono<UserModel> getUser(@PathVariable int userId) {
        return userService.getByID(userId);
    }

    @GetMapping("/{userId}/conferences/{conferenceId}/status")
    public Mono<String> getUserConferenceStatus(@PathVariable int userId, @PathVariable int conferenceId) {
        return userService.getUserConferenceStatus(conferenceId, userId);
    }
}
