package com.fon.confms.controller.user;

import com.fon.confms.model.TitleModel;
import com.fon.confms.service.TitleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/users/titles")
public class UserTitleController {

    @Resource
    private TitleService titleService;

    @GetMapping
    public Flux<TitleModel> getAll() {
        return titleService.getAll();
    }
}
