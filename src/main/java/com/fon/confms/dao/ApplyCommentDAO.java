package com.fon.confms.dao;

import com.fon.confms.model.ApplicationCommentModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ApplyCommentDAO extends ReactiveCrudRepository<ApplicationCommentModel, Integer> {

    String BASIC_QUERY = "SELECT rc.* "
        + "FROM `ApplicationComment` rc "
        + "JOIN Apply a on rc.apply = a.id";

    @Query(BASIC_QUERY)
    Flux<ApplicationCommentModel> findAll();

    @Query(BASIC_QUERY + " WHERE rc.apply = :applicationId ORDER BY rc.dateTime ASC")
    Flux<ApplicationCommentModel> findByApply(@Param("applicationId") int applicationId);

    @Query(BASIC_QUERY + " WHERE rc.user = :id ORDER BY rc.dateTime ASC")
    Mono<ApplicationCommentModel> findByUser(@Param("id") int id);
}
