package com.fon.confms.dao;

import com.fon.confms.model.ApplyModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ApplyDAO extends ReactiveCrudRepository<ApplyModel, Integer> {

    String BASIC_QUERY = "SELECT a.* FROM `Apply` a";

    String JOIN_DOCUMENT_QUERY =
        "SELECT a.*, d.name as documentName, d.description as documentDescription, d.createdAt as documentCreatedAt, "
            + "d.file as documentFile, d.fileName as documentFileName, d.user, d.conference, d.category, d.section "
            + "FROM `Apply` a "
            + "JOIN Document d on a.document = d.id";

    @Query(BASIC_QUERY)
    Flux<ApplyModel> findAll();

    @Query(JOIN_DOCUMENT_QUERY + " WHERE a.id = :id")
    Mono<ApplyModel> findById(@Param("id") int id);

    @Query(JOIN_DOCUMENT_QUERY + " WHERE a.id = :id AND d.conference = :conferenceId")
    Mono<ApplyModel> findByIdAndConference(@Param("id") int id, @Param("conferenceId") int conferenceId);

    @Query(JOIN_DOCUMENT_QUERY + " WHERE d.conference = :conferenceid and d.user = :userId")
    Flux<ApplyModel> findByUserAndConference(@Param("conferenceid") int conferenceid, @Param("userId") int userId);

    @Query(JOIN_DOCUMENT_QUERY + " WHERE d.conference = :conferenceId and d.section = :sectionId")
    Flux<ApplyModel> findByConferenceSection(@Param("conferenceId") int conferenceId, @Param("sectionId") int sectionId);

    @Query("UPDATE Apply SET status = :status WHERE id = :appId")
    Mono<Void> updateStatus(@Param("status") String status, @Param("appId") int appId);
}
