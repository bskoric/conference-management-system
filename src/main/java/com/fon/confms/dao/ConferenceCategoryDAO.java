package com.fon.confms.dao;

import com.fon.confms.model.ConferenceCategoryModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ConferenceCategoryDAO extends ReactiveCrudRepository<ConferenceCategoryModel, Integer> {

}
