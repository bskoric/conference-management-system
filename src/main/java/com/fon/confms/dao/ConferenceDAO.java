package com.fon.confms.dao;

import com.fon.confms.model.ConferenceModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ConferenceDAO extends ReactiveCrudRepository<ConferenceModel, Integer> {

    String BASIC_QUERY =
        "SELECT con.*, c.name as countryName, c.isoCode as countryIsoCode, cc.name as categoryName, cc.description as categoryDesc "
            + "FROM `Conference` con JOIN Country c ON con.country = c.id join ConferenceCategory cc on con.category = cc.id";

    @Query(BASIC_QUERY)
    Flux<ConferenceModel> findAll();

    @Query(BASIC_QUERY + " WHERE con.id = :id")
    Mono<ConferenceModel> findById(@Param("id") int id);

    @Query(BASIC_QUERY + " WHERE c.id = :countryId")
    Mono<ConferenceModel> findByCountry(@Param("countryId") int countryId);

    @Query("SELECT count(1) FROM `RegisterToConference` WHERE user = :userId AND conference = :conferenceId")
    Mono<Integer> isUserRegisterToConferenceApp(@Param("conferenceId") int conferenceId, @Param("userId") int userId);

    @Query("INSERT INTO `RegisterToConference`(user, conference, status) VALUES (:userId,:conferenceId,:status)")
    Mono<Integer> registerToConference(@Param("conferenceId") int conference, @Param("userId") int user, @Param("status") String status);

    @Query("SELECT con.*, c.name as countryName, c.isoCode as countryIsoCode, cc.name as categoryName, cc.description as categoryDesc "
               + "FROM `RegisterToConference` rc "
               + "JOIN Conference con on con.id = rc.conference "
               + "JOIN Country c ON con.country = c.id "
               + "JOIN ConferenceCategory cc on con.category = cc.id "
               + "WHERE user = :userId")
    Flux<ConferenceModel> findConferencesForUser(@Param("userId") int userId);
}
