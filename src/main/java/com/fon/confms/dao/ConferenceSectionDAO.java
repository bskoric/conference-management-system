package com.fon.confms.dao;

import com.fon.confms.model.ConferenceSectionModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ConferenceSectionDAO extends ReactiveCrudRepository<ConferenceSectionModel, Integer> {

    String BASIC_QUERY =
        "SELECT sec.*, cc.name as categoryName, cc.description as categoryDesc "
            + "FROM `ConferenceSection` sec JOIN ConferenceCategory cc ON sec.category = cc.id";

    String QUERY_CONFERENCE_SECTION_TO_CONFERENCE =
        "SELECT sec.id as 'id', sec.name as 'name', cc.id as category, cc.name as categoryName, cc.description as categoryDesc\n"
            + "FROM `ConferenceSection2Conference` con2sec "
            + "JOIN ConferenceSection sec on con2sec.section = sec.id "
            + "JOIN ConferenceCategory cc on sec.category = cc.id";

    String QUERY_CONFERENCE_SECTION_TO_MEMBER =
        "SELECT sec.id as 'id', sec.name as 'name', cc.id as category, cc.name as categoryName, cc.description as categoryDesc "
            + "FROM UserCommitteeMember2ConferenceSection cm2s "
            + "JOIN ConferenceSection sec on cm2s.section = sec.id "
            + "JOIN ConferenceCategory cc on sec.category = cc.id";

    @Query(BASIC_QUERY)
    Flux<ConferenceSectionModel> findAll();

    @Query(BASIC_QUERY + " WHERE sec.id = :id")
    Mono<ConferenceSectionModel> findById(@Param("id") int id);

    @Query(QUERY_CONFERENCE_SECTION_TO_CONFERENCE + " WHERE conference = :conferenceID")
    Flux<ConferenceSectionModel> findSectionsForConference(@Param("conferenceID") int conferenceId);

    @Query(BASIC_QUERY + " WHERE sec.category = :categoryId")
    Flux<ConferenceSectionModel> findSectionsForCategory(@Param("categoryId") int categoryId);

    @Query("SELECT count(1) FROM `ConferenceSection2Conference` WHERE section = :sectionId AND conference = :conferenceId")
    Mono<Integer> isSectionBelongsToConference(@Param("conferenceId") int conferenceId, @Param("sectionId") int sectionId);

    @Query(QUERY_CONFERENCE_SECTION_TO_MEMBER + " WHERE cm2s.committeeMember = :member")
    Flux<ConferenceSectionModel> findSectionsForCommitteeMember(@Param("member") int member);
}
