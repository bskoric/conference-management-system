package com.fon.confms.dao;

import com.fon.confms.model.CountryModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryDAO extends ReactiveCrudRepository<CountryModel, Integer> {

}
