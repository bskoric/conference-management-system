package com.fon.confms.dao;

import com.fon.confms.model.DocumentCategoryModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface DocumentCategoryDAO extends ReactiveCrudRepository<DocumentCategoryModel, Integer> {

}
