package com.fon.confms.dao;

import com.fon.confms.model.DocumentModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DocumentDAO extends ReactiveCrudRepository<DocumentModel, Integer> {

    String BASIC_QUERY =
        "SELECT d.*, dc.name as 'categoryName', dc.description as 'categoryDescription', "
            + "u.name as 'userName', u.surname 'userSurname', u.email as 'userEmail', u.username as 'userUsername', "
            + "u.password as 'userPassword', u.title as 'userTitle', u.country as 'userCountry', u.role as 'userRole',\n"
            + "con.id as 'conferenceId', con.name as 'conferenceName', con.town as 'conferenceTown', con.closed as 'conferenceIsClosed', "
            + "con.date as 'conferenceDate', con.country as 'conferenceCountry', con.category as 'conferenceCategory' \n"
            + "FROM `Document` d "
            + "JOIN User u on d.user = u.id "
            + "JOIN Conference con on d.conference = con.id "
            + "JOIN DocumentCategory dc ON d.category = dc.id";

    @Query(BASIC_QUERY)
    Flux<DocumentModel> findAll();

    @Query(BASIC_QUERY + " WHERE d.id = :id")
    Mono<DocumentModel> findById(@Param("id") int id);
}
