package com.fon.confms.dao;

import com.fon.confms.model.ReviewModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReviewDAO extends ReactiveCrudRepository<ReviewModel, Integer> {

    String BASIC_QUERY = "SELECT r.* FROM `Review` r";

    String APPLY_JOIN_QUERY = "SELECT r.*, a.status as 'applyStatus', a.document, a.conferenceMember, "
        + "d.name as documentName, d.description as documentDescription, d.createdAt as documentCreatedAt, d.file as documentFile, "
        + "d.fileName as documentFileName, d.user, d.conference, d.category, d.section, "
        + "u.name as 'cm_userName', u.surname 'cm_userSurname', u.email as 'cm_userEmail', u.username as 'cm_userUsername', "
        + "u.password as 'cm_userPassword', u.title as 'cm_userTitle', u.country as 'cm_userCountry', u.role as 'cm_userRole', u.id as cm_user "
        + "FROM `Review` r "
        + "JOIN Apply a on r.apply = a.id "
        + "JOIN Document d on a.document = d.id "
        + "JOIN User u on r.committeeMember = u.id";

    @Query(BASIC_QUERY)
    Flux<ReviewModel> findAll();

    @Query(APPLY_JOIN_QUERY + " WHERE r.id = :id")
    Mono<ReviewModel> findById(@Param("id") int id);

    @Query(APPLY_JOIN_QUERY + " WHERE r.committeeMember = :committeeMemberId AND a.status = :applyStatus AND d.conference = :conferenceId")
    Flux<ReviewModel> findByCommitteeMemberIdAndApplyStatus(@Param("committeeMember") int committeeMemberId,
                                                            @Param("conferenceId") int conferenceId,
                                                            @Param("applyStatus") String applyStatus);

    @Query(APPLY_JOIN_QUERY + " WHERE r.committeeMember = :committeeMemberId AND r.apply = :applyId")
    Mono<ReviewModel> findByCommitteeMemberIdAndApply(@Param("committeeMember") int committeeMemberId,
                                                      @Param("applyId") int applyId);

    @Query(APPLY_JOIN_QUERY + " WHERE r.apply = :id")
    Flux<ReviewModel> findByApply(@Param("id") int id);

    @Query(BASIC_QUERY + " WHERE r.status = :status")
    Mono<ReviewModel> findByStatus(@Param("status") String status);

    @Query(APPLY_JOIN_QUERY + " where a.conferenceMember = :userId and d.conference = :conferenceId")
    Flux<ReviewModel> findByConferenceMember(@Param("userId") int userId, @Param("conferenceId") int conferenceId);

    @Query("UPDATE Review SET status = :status WHERE id = :reviewId")
    Mono<Void> updateStatus(@Param("status") String status, @Param("reviewId") int reviewId);
}
