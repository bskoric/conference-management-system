package com.fon.confms.dao;

import com.fon.confms.model.RoleModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface RoleDAO extends ReactiveCrudRepository<RoleModel, Integer> {

    String BASIC_QUERY = "SELECT u.* FROM UserRole u";

    @Query(BASIC_QUERY + " WHERE u.name = :name")
    Mono<RoleModel> findByName(@Param("name") String name);
}
