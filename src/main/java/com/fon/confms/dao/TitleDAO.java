package com.fon.confms.dao;

import com.fon.confms.model.TitleModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface TitleDAO extends ReactiveCrudRepository<TitleModel, Integer> {

}
