package com.fon.confms.dao;

import com.fon.confms.model.UserModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserDAO extends ReactiveCrudRepository<UserModel, Integer> {

    String BASIC_QUERY =
        "SELECT DISTINCT u.*, c.id as countryId, c.name as countryName, c.isoCode as countryIsoCode, r.name as roleName, t.name as titleName "
            + "FROM `User` u "
            + "join Country c ON u.country = c.id "
            + "join UserRole r on u.role = r.id "
            + "join Title t on u.title = t.id ";

    String CONFERENCE_QUERY = BASIC_QUERY
        + "join RegisterToConference reg on reg.user = u.id "
        + "join Conference con on con.id = reg.conference";

    @Query(BASIC_QUERY)
    Flux<UserModel> findAll();

    @Query(BASIC_QUERY + " WHERE u.id = :id")
    Mono<UserModel> findById(@Param("id") int id);

    @Query(BASIC_QUERY + " WHERE username = :username")
    Mono<UserModel> findByUsername(@Param("username") String username);

    @Query(CONFERENCE_QUERY + " WHERE con.id = :conferenceId AND r.name = 'USER' AND reg.status = 'CONFERENCE_APPROVED'")
    Flux<UserModel> findConferenceUserByConference(@Param("conferenceId") int conferenceId);

    @Query(CONFERENCE_QUERY + " WHERE con.id = :conferenceId AND r.name = 'COMMITTEE_MEMBER' AND reg.status = 'CONFERENCE_APPROVED'")
    Flux<UserModel> findCommitteeMemberUserByConference(@Param("conferenceId") int conferenceId);

    @Query("SELECT status FROM `RegisterToConference` WHERE user = :userId and conference = :conferenceId")
    Mono<String> findUserConferenceStatus(@Param("conferenceId") int conferenceId, @Param("userId") int userId);
}
