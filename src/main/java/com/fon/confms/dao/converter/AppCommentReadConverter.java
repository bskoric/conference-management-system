package com.fon.confms.dao.converter;

import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.UserModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDateTime;

@ReadingConverter
public class AppCommentReadConverter implements Converter<Row, ApplicationCommentModel> {

    @Override
    public ApplicationCommentModel convert(Row source) {
        return ApplicationCommentModel.builder()
                                      .id(source.get("id", Integer.class))
                                      .text(source.get("text", String.class))
                                      .apply(getApply(source))
                                      .user(UserModel.builder().id(source.get("user", Integer.class)).build())
                                      .dateTime(source.get("dateTime", LocalDateTime.class))
                                      .build();
    }

    private ApplyModel getApply(Row source) {
        return ApplyModel.builder()
                          .id(source.get("id", Integer.class))
                          .build();
    }
}
