package com.fon.confms.dao.converter;

import com.fon.confms.model.ApplicationCommentModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

@WritingConverter
public class AppCommentWriteConverter implements Converter<ApplicationCommentModel, OutboundRow> {

    @Override
    public OutboundRow convert(ApplicationCommentModel applyModel) {
        OutboundRow row = new OutboundRow();

        row.put("apply", Parameter.from(applyModel.getApply().getId()));
        row.put("user", Parameter.from(applyModel.getUser().getId()));
        row.put("text", Parameter.from(applyModel.getText()));
        row.put("dateTime", Parameter.from(applyModel.getDateTime()));
        return row;
    }
}
