package com.fon.confms.dao.converter;

import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class ApplyReadConverter implements Converter<Row, ApplyModel> {

    @Override
    public ApplyModel convert(Row source) {
        return ApplyModel.builder()
                         .id(source.get("id", Integer.class))
                         .document(ReviewReadConverter.getDocument(source))
                         .conferenceMember(UserModel.builder().id(source.get("conferenceMember", Integer.class)).build())
                         .status(ApplyStatusEnum.valueOf(source.get("status", String.class)))
                         .build();
    }
}
