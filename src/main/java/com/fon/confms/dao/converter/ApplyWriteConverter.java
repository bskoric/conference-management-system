package com.fon.confms.dao.converter;

import com.fon.confms.model.ApplyModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

@WritingConverter
public class ApplyWriteConverter implements Converter<ApplyModel, OutboundRow> {

    @Override
    public OutboundRow convert(ApplyModel applyModel) {
        OutboundRow row = new OutboundRow();

        row.put("conferenceMember", Parameter.from(applyModel.getConferenceMember().getId()));
        row.put("document", Parameter.from(applyModel.getDocument().getId()));
        row.put("status", Parameter.from(applyModel.getStatus().toString()));
        return row;
    }
}
