package com.fon.confms.dao.converter;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.CountryModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDate;

@ReadingConverter
public class ConferenceReadConverter implements Converter<Row, ConferenceModel> {

    @Override
    public ConferenceModel convert(Row source) {
        return ConferenceModel.builder()
                              .id(source.get("id", Integer.class))
                              .name(source.get("name", String.class))
                              .date(source.get("date", LocalDate.class))
                              .town(source.get("town", String.class))
                              .isClosed(source.get("closed", Boolean.class))
                              .country(getCountry(source))
                              .category(getConferenceCategory(source))
                              .description(source.get("description", String.class))
                              .build();
    }

    private CountryModel getCountry(Row source) {
        CountryModel country = new CountryModel();
        country.setId(source.get("country", Integer.class));
        country.setName(source.get("countryName", String.class));
        country.setIsoCode(source.get("countryIsoCode", String.class));
        return country;
    }

    private ConferenceCategoryModel getConferenceCategory(Row source) {
        return ConferenceCategoryModel.builder()
                                      .id(source.get("category", Integer.class))
                                      .name(source.get("categoryName", String.class))
                                      .description(source.get("categoryDesc", String.class))
                                      .build();
    }
}
