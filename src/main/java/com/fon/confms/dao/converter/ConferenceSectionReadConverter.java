package com.fon.confms.dao.converter;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceSectionModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class ConferenceSectionReadConverter implements Converter<Row, ConferenceSectionModel> {

    @Override
    public ConferenceSectionModel convert(Row source) {
        return ConferenceSectionModel.builder()
                                     .id(source.get("id", Integer.class))
                                     .name(source.get("name", String.class))
                                     .category(getConferenceCategory(source))
                                     .build();
    }

    private ConferenceCategoryModel getConferenceCategory(Row source) {
        return ConferenceCategoryModel.builder()
                                      .id(source.get("category", Integer.class))
                                      .name(source.get("categoryName", String.class))
                                      .description(source.get("categoryDesc", String.class))
                                      .build();
    }
}
