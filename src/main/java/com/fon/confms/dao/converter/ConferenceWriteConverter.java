package com.fon.confms.dao.converter;

import com.fon.confms.model.ConferenceModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

@WritingConverter
public class ConferenceWriteConverter implements Converter<ConferenceModel, OutboundRow> {

    @Override
    public OutboundRow convert(ConferenceModel conferenceModel) {
        OutboundRow row = new OutboundRow();

        row.put("id", Parameter.from(conferenceModel.getId()));
        row.put("name", Parameter.from(conferenceModel.getName()));
        row.put("date", Parameter.from(conferenceModel.getDate()));
        row.put("town", Parameter.from(conferenceModel.getTown()));
        row.put("isClosed", Parameter.from(conferenceModel.isClosed()));
        row.put("category", Parameter.from(conferenceModel.getCategory().getId()));
        row.put("country", Parameter.from(conferenceModel.getCountry().getId()));
        return row;
    }
}
