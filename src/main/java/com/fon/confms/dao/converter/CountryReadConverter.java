package com.fon.confms.dao.converter;

import com.fon.confms.model.CountryModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class CountryReadConverter implements Converter<Row, CountryModel> {

    @Override
    public CountryModel convert(Row source) {
        return CountryModel.builder()
                           .id(source.get("id", Integer.class))
                           .name((source.get("name", String.class)))
                           .isoCode(source.get("isoCode", String.class))
                           .build();
    }
}
