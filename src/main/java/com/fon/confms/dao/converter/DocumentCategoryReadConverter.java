package com.fon.confms.dao.converter;

import com.fon.confms.model.DocumentCategoryModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class DocumentCategoryReadConverter implements Converter<Row, DocumentCategoryModel> {

    @Override
    public DocumentCategoryModel convert(Row source) {
        return DocumentCategoryModel.builder()
                           .id(source.get("id", Integer.class))
                           .name((source.get("name", String.class)))
                           .description(source.get("description", String.class))
                           .build();
    }
}
