package com.fon.confms.dao.converter;

import com.fon.confms.dao.converter.helper.ConverterHelper;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDate;

@ReadingConverter
public class DocumentReadConverter implements Converter<Row, DocumentModel> {

    @Override
    public DocumentModel convert(Row source) {
        return DocumentModel.builder()
                            .id(source.get("id", Integer.class))
                            .name(source.get("name", String.class))
                            .description(source.get("description", String.class))
                            .createdAt(source.get("createdAt", LocalDate.class))
                            .file(source.get("file", byte[].class))
                            .fileName(source.get("fileName", String.class))
                            .user(ConverterHelper.getUser(source,""))
                            .conference(ConverterHelper.getConference(source))
                            .category(getDocumentCategory(source))
                            .section(ConferenceSectionModel.builder().id(source.get("section", Integer.class)).build())
                            .build();
    }

    private DocumentCategoryModel getDocumentCategory(Row source) {
        return DocumentCategoryModel.builder()
                                    .id(source.get("category", Integer.class))
                                    .name(source.get("categoryName", String.class))
                                    .description(source.get("categoryDescription", String.class))
                                    .build();
    }
}
