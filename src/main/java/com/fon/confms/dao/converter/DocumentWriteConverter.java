package com.fon.confms.dao.converter;

import com.fon.confms.model.DocumentModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

@WritingConverter
public class DocumentWriteConverter implements Converter<DocumentModel, OutboundRow> {

    @Override
    public OutboundRow convert(DocumentModel documentModel) {
        OutboundRow row = new OutboundRow();

        row.put("id", Parameter.from(documentModel.getId()));
        row.put("name", Parameter.from(documentModel.getName()));
        row.put("description", Parameter.from(documentModel.getDescription()));
        row.put("createdAt", Parameter.from(documentModel.getCreatedAt()));
        row.put("file", Parameter.from(documentModel.getFile()));
        row.put("fileName", Parameter.from(documentModel.getFileName()));
        row.put("user", Parameter.from(documentModel.getUser().getId()));
        row.put("conference", Parameter.from(documentModel.getConference().getId()));
        row.put("category", Parameter.from(documentModel.getCategory().getId()));
        row.put("section", Parameter.from(documentModel.getSection().getId()));
        return row;
    }
}
