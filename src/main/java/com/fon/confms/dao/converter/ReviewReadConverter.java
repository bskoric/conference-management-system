package com.fon.confms.dao.converter;

import com.fon.confms.dao.converter.helper.ConverterHelper;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import io.r2dbc.spi.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@ReadingConverter
public class ReviewReadConverter implements Converter<Row, ReviewModel> {

    private final static Logger LOG = LoggerFactory.getLogger(ReviewReadConverter.class);

    @Override
    public ReviewModel convert(Row source) {
        return ReviewModel.builder()
                          .id(source.get("id", Integer.class))
                          .apply(getApply(source))
                          .committeeMember(getUser(source))
                          .status(ReviewStatusEnum.valueOf(source.get("status", String.class)))
                          .build();
    }

    private ApplyModel getApply(Row source) {
        try {
            return ApplyModel.builder()
                             .id(source.get("apply", Integer.class))
                             .status(ApplyStatusEnum.valueOf(source.get("applyStatus", String.class)))
                             .document(getDocument(source))
                             .conferenceMember(UserModel.builder().id(source.get("conferenceMember", Integer.class)).build())
                             .build();
        } catch (NoSuchElementException ex) {
            LOG.error(ex.getMessage());
            return ApplyModel.builder()
                             .id(source.get("apply", Integer.class))
                             .build();
        }
    }

    private UserModel getUser(Row source) {
        try {
            return ConverterHelper.getUser(source, "cm_");
        } catch (NoSuchElementException ex) {
            LOG.error(ex.getMessage());
            return UserModel.builder().id(source.get("committeeMember", Integer.class)).build();
        }
    }

    public static DocumentModel getDocument(Row source) {
        try {
            return DocumentModel.builder()
                                .id(source.get("document", Integer.class))
                                .name(source.get("documentName", String.class))
                                .description(source.get("documentDescription", String.class))
                                .createdAt(source.get("documentCreatedAt", LocalDate.class))
                                .file(source.get("documentFile", byte[].class))
                                .fileName(source.get("documentFileName", String.class))
                                .user(UserModel.builder().id(source.get("user", Integer.class)).build())
                                .category(DocumentCategoryModel.builder().id(source.get("category", Integer.class)).build())
                                .section(ConferenceSectionModel.builder().id(source.get("section", Integer.class)).build())
                                .conference(ConferenceModel.builder().id(source.get("conference", Integer.class)).build())
                                .build();
        } catch (NoSuchElementException ex) {
            LOG.error(ex.getMessage());
            return DocumentModel.builder()
                                .id(source.get("document", Integer.class))
                                .build();
        }
    }
}
