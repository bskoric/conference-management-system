package com.fon.confms.dao.converter;

import com.fon.confms.model.ReviewModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

public class ReviewWriteConverter implements Converter<ReviewModel, OutboundRow> {

    @Override
    public OutboundRow convert(ReviewModel review) {
        OutboundRow row = new OutboundRow();

        row.put("id", Parameter.from(review.getId()));
        row.put("committeeMember", Parameter.from(review.getCommitteeMember().getId()));
        row.put("apply", Parameter.from(review.getApply().getId()));
        row.put("status", Parameter.from(review.getStatus().toString()));
        return row;
    }

}
