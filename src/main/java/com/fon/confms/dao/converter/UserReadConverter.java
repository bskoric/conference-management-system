package com.fon.confms.dao.converter;

import com.fon.confms.dao.converter.helper.UserConverterHelper;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.UserModel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class UserReadConverter implements Converter<Row, UserModel> {

    @Override
    public UserModel convert(Row source) {
        return UserModel.builder()
                        .id(source.get("id", Integer.class))
                        .name(source.get("name", String.class))
                        .surname(source.get("surname", String.class))
                        .email(source.get("email", String.class))
                        .username(source.get("username", String.class))
                        .password(source.get("password", String.class))
                        .country(UserConverterHelper.getCountry(source))
                        .role(UserConverterHelper.getRole(source))
                        .title(UserConverterHelper.getTitle(source))
                        .build();
    }
}
