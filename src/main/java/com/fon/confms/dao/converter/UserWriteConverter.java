package com.fon.confms.dao.converter;

import com.fon.confms.dao.converter.helper.UserConverterHelper;
import com.fon.confms.model.UserModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

@WritingConverter
public class UserWriteConverter implements Converter<UserModel, OutboundRow> {

    @Override
    public OutboundRow convert(UserModel user) {
        return UserConverterHelper.getWriteRow(user);
    }
}
