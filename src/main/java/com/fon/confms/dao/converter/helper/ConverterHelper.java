package com.fon.confms.dao.converter.helper;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.CountryModel;
import com.fon.confms.model.RoleModel;
import com.fon.confms.model.TitleModel;
import com.fon.confms.model.UserModel;
import io.r2dbc.spi.Row;

import java.time.LocalDate;

public final class ConverterHelper {

    private ConverterHelper() {
    }

    public static final String CONFERENCE_ID = "conferenceId";
    public static final String CONFERENCE_NAME = "conferenceName";
    public static final String CONFERENCE_DATE = "conferenceDate";
    public static final String CONFERENCE_CLOSED = "conferenceIsClosed";
    public static final String CONFERENCE_TOWN = "conferenceTown";
    public static final String CONFERENCE_CATEGORY = "conferenceCategory";
    public static final String CONFERENCE_COUNTRY = "conferenceCountry";

    public static ConferenceModel getConference(Row source) {
        return ConferenceModel.builder()
                              .id(source.get(CONFERENCE_ID, Integer.class))
                              .name(source.get(CONFERENCE_NAME, String.class))
                              .date(source.get(CONFERENCE_DATE, LocalDate.class))
                              .town(source.get(CONFERENCE_TOWN, String.class))
                              .isClosed(source.get(CONFERENCE_CLOSED, Boolean.class))
                              .category(getConferenceCategory(source))
                              .country(getCountry(source, "conference"))
                              .description(source.get("description", String.class))
                              .build();
    }

    public static UserModel getUser(Row source, String prefix) {
        return UserModel.builder()
                        .id(source.get(prefix + "user", Integer.class))
                        .name(source.get(prefix + "userName", String.class))
                        .surname(source.get(prefix + "userSurname", String.class))
                        .email(source.get(prefix + "userEmail", String.class))
                        .username(source.get(prefix + "userUsername", String.class))
                        .password(source.get(prefix + "userPassword", String.class))
                        .country(getCountry(source, prefix + "user"))
                        .title(getTitle(source, prefix))
                        .role(getRole(source, prefix))
                        .build();
    }

    private static CountryModel getCountry(Row source, String type) {
        return CountryModel.builder().id(source.get(type + "Country", Integer.class)).build();
    }

    private static TitleModel getTitle(Row source, String prefix) {
        return TitleModel.builder().id(source.get(prefix + "userTitle", Integer.class)).build();
    }

    private static RoleModel getRole(Row source, String prefix) {
        return RoleModel.builder().id(source.get(prefix + "userRole", Integer.class)).build();
    }

    private static ConferenceCategoryModel getConferenceCategory(Row source) {
        return ConferenceCategoryModel.builder().id(source.get(CONFERENCE_CATEGORY, Integer.class)).build();
    }
}
