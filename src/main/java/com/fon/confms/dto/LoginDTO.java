package com.fon.confms.dto;

import com.fon.confms.model.UserModel;

public class LoginDTO {

    private String accessToken;

    private UserModel user;

    private boolean error;

    private String errorMessage;

    public LoginDTO() {
    }

    public LoginDTO(String accessToken, UserModel user) {
        this.accessToken = accessToken;
        this.user = user;
    }

    public LoginDTO(boolean error, String errorMessage) {
        this.error = error;
        this.errorMessage = errorMessage;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
