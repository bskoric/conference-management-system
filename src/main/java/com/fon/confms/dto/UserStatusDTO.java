package com.fon.confms.dto;

public class UserStatusDTO {

    private boolean isUserRegisterToConferenceApp;

    private String status;

    public UserStatusDTO(boolean isUserRegister, String status) {
        this.isUserRegisterToConferenceApp = isUserRegister;
        this.status = status;
    }

    public UserStatusDTO(boolean isUserRegister) {
        this.isUserRegisterToConferenceApp = isUserRegister;
    }

    public UserStatusDTO(String status) {
        this.status = status;
    }

    public boolean isUserRegisterToConferenceApp() {
        return isUserRegisterToConferenceApp;
    }

    public void setUserRegisterToConferenceApp(boolean userRegisterToConferenceApp) {
        isUserRegisterToConferenceApp = userRegisterToConferenceApp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
