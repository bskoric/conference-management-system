package com.fon.confms.dto.request;

import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.UserModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateReviewRequestDTO {

    private UserModel committeeMember;
    private ApplyModel application;
}
