package com.fon.confms.model;

import com.fon.confms.model.enums.ApplyStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("Apply")
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplyModel {

    @Id
    private int id;

    private UserModel conferenceMember;

    private DocumentModel document;

    private ApplyStatusEnum status;
}
