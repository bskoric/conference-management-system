package com.fon.confms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("ConferenceSection")
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceSectionModel {

    @Id
    private int id;

    private String name;

    private ConferenceCategoryModel category;
}
