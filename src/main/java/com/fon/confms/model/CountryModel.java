package com.fon.confms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Table("Country")
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryModel implements Serializable {

    @Id
    private int id;

    private String name;

    private String isoCode;
}
