package com.fon.confms.model;

import com.fon.confms.model.enums.ReviewStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("Review")
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewModel {

    @Id
    private int id;

    private UserModel committeeMember;

    private ApplyModel apply;

    private ReviewStatusEnum status;
}
