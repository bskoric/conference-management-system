package com.fon.confms.model.enums;

public enum ApplyStatusEnum {
    OPENED,
    IN_REVIEW,
    APPLIED,
    DECLINED,
    CLOSED
}
