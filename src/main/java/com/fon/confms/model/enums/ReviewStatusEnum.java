package com.fon.confms.model.enums;

public enum ReviewStatusEnum {
    APPROVED,
    DECLINED,
    IN_PROGRESS
}
