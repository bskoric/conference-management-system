package com.fon.confms.model.enums;

public enum UserStatusEnum {
    CONFERENCE_DECLINED,
    CONFERENCE_APPROVED,
    PENDING_APP_REGISTRATION,
    REGISTERED_TO_APP;
}
