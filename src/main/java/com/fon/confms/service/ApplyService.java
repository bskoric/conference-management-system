package com.fon.confms.service;

import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.DocumentModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ApplyService extends GenericService<ApplyModel> {

    Mono<ApplyModel> createApply(DocumentModel document);

    Mono<Boolean> activeApplyNotExist(DocumentModel document);

    Flux<ApplyModel> getByUserAndConference(int conferenceId, int userId);

    Mono<ApplyModel> getApply(int applyId, int conferenceId);

    Flux<ApplicationCommentModel> getCommentsForApplication(int applicationId);

    Mono<ApplicationCommentModel> createComment(ApplicationCommentModel comment);

    Mono<Void> deleteComment(int commentId, int userId);

    Mono<Void> closeApplication(int applicationId);

    Mono<Void> deleteApplication(int applicationId);

    Mono<Void> finishApplication(int applicationId);

    Flux<ApplyModel> getApplicationsForCommitteeMember(int conferenceId, int committeeMemberId);
}
