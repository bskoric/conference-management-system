package com.fon.confms.service;

import com.fon.confms.model.CountryModel;

public interface CountryService extends GenericService<CountryModel> {

}
