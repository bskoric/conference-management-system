package com.fon.confms.service;

import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DocumentService extends GenericService<DocumentModel> {

    Flux<DocumentCategoryModel> getCategories();

    Mono<DocumentCategoryModel> getCategory(int categoryId);

    Mono<DocumentModel> createDocument(DocumentModel document);
}
