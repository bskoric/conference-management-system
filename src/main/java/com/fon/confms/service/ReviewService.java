package com.fon.confms.service;

import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ReviewStatusEnum;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReviewService extends GenericService<ReviewModel> {

    Mono<ReviewModel> getReview(int committeeMemberId, int applyID);

    Flux<ReviewModel> getReviews(int userId, int conferenceId);

    Flux<ReviewModel> getReviews(int committeeMember, int conferenceId, String applyStatus);

    Flux<ReviewModel> getReviews(int applyID);

    Mono<Void> changeStatus(int reviewId, ReviewStatusEnum status);

    Mono<ReviewModel> createReview(ApplyModel application, UserModel committeeMember);
}
