package com.fon.confms.service;

import com.fon.confms.model.RoleModel;
import reactor.core.publisher.Mono;

public interface RoleService extends GenericService<RoleModel> {

    Mono<RoleModel> getByName(String roleName);
}
