package com.fon.confms.service;

import com.fon.confms.model.TitleModel;

public interface TitleService extends GenericService<TitleModel> {

}
