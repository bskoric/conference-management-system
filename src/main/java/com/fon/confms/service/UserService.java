package com.fon.confms.service;

import com.fon.confms.model.UserModel;
import reactor.core.publisher.Mono;

public interface UserService extends GenericService<UserModel> {
    Mono<UserModel> getByUsername(String username);

    Mono<String> getUserConferenceStatus(int conferenceId, int userId);

    Mono<UserModel> register(UserModel user);
}
