package com.fon.confms.service.conference;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.service.GenericService;

public interface ConferenceCategoryService extends GenericService<ConferenceCategoryModel> {

}
