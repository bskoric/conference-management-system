package com.fon.confms.service.conference;

import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.service.GenericService;
import reactor.core.publisher.Flux;

public interface ConferenceSectionService extends GenericService<ConferenceSectionModel> {

    Flux<ConferenceSectionModel> getSectionsForConference(int conferenceId);

    Flux<ConferenceSectionModel> getSectionsForCategory(int categoryId);

    Flux<ConferenceSectionModel> getSectionsForCommitteeMember(int committeeMemberId);
}
