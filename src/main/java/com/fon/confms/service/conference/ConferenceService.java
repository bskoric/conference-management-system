package com.fon.confms.service.conference;

import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.service.GenericService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ConferenceService extends GenericService<ConferenceModel> {

    Flux<UserModel> getConferenceMembers(int conferenceId);

    Flux<UserModel> getCommitteeMembers(int conferenceId);

    Mono<Boolean> isUserRegisterToConferenceApp(int conferenceId, int userId);

    Mono<Boolean> registerToConferenceApp(int conferenceId, int userId);

    Flux<ConferenceModel> findConferencesForUser(UserModel user);
}
