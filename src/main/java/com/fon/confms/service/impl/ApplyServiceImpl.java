package com.fon.confms.service.impl;

import com.fon.confms.dao.ApplyCommentDAO;
import com.fon.confms.dao.ApplyDAO;
import com.fon.confms.dao.ConferenceSectionDAO;
import com.fon.confms.dao.ReviewDAO;
import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import com.fon.confms.service.ApplyService;
import com.fon.confms.service.DocumentService;
import com.fon.confms.service.conference.ConferenceService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

import javax.annotation.Resource;

@Service
public class ApplyServiceImpl implements ApplyService {

    @Resource
    private ApplyDAO applyDAO;

    @Resource
    private DocumentService documentService;

    @Resource
    private ApplyCommentDAO applyCommentDAO;

    @Resource
    private ReviewDAO reviewDAO;

    @Resource
    private ConferenceSectionDAO conferenceSectionDAO;

    @Resource
    private ConferenceService conferenceService;

    @Override
    public Mono<ApplyModel> createApply(DocumentModel document) {
        return documentService.createDocument(document).flatMap(documentModel -> applyDAO.save(createApplyModel(documentModel)));
    }

    @Override
    public Mono<Boolean> activeApplyNotExist(DocumentModel document) {
        return applyDAO.findByUserAndConference(document.getConference().getId(), document.getUser().getId())
                       .map(ApplyModel::getStatus)
                       .all(status -> Objects.equals(status, ApplyStatusEnum.DECLINED));
    }

    @Override
    public Flux<ApplyModel> getByUserAndConference(int conferenceId, int userId) {
        return applyDAO.findByUserAndConference(conferenceId, userId);
    }

    @Override
    public Flux<ApplyModel> getAll() {
        return applyDAO.findAll();
    }

    @Override
    public Mono<ApplyModel> getByID(int id) {
        return applyDAO.findById(id);
    }

    @Override
    public Mono<ApplyModel> getApply(int applyId, int conferenceId) {
        return applyDAO.findByIdAndConference(applyId, conferenceId);
    }

    @Override
    public Flux<ApplicationCommentModel> getCommentsForApplication(int applicationId) {
        return applyCommentDAO.findByApply(applicationId);
    }

    @Override
    public Mono<ApplicationCommentModel> createComment(ApplicationCommentModel comment) {
        return applyCommentDAO.save(comment);
    }

    @Override
    public Mono<Void> deleteComment(int commentId, int userId) {
        return applyCommentDAO.findById(commentId)
                              .filter(comment -> comment.getUser().getId() == userId)
                              .flatMap(comment -> applyCommentDAO.delete(comment));
    }

    @Override
    public Mono<Void> closeApplication(int applicationId) {
        return applyDAO.updateStatus(ApplyStatusEnum.CLOSED.toString(), applicationId);
    }

    @Override
    public Mono<Void> deleteApplication(int applicationId) {
        return applyDAO.deleteById(applicationId);
    }

    @Override
    public Mono<Void> finishApplication(int applicationId) {
        return reviewDAO.findByApply(applicationId)
                        .map(ReviewModel::getStatus)
                        .filter(status -> status.equals(ReviewStatusEnum.APPROVED))
                        .count()
                        .map(result -> result >= 2)
                        .flatMap(approved -> {
                            if (approved) {
                                return applyDAO.updateStatus(ApplyStatusEnum.APPLIED.toString(), applicationId);
                            } else {
                                return Mono.error(new Exception("You must have 2 approvals"));
                            }
                        });
    }

    @Override
    public Flux<ApplyModel> getApplicationsForCommitteeMember(int conferenceId, int committeeMemberId) {
        return conferenceService.isUserRegisterToConferenceApp(conferenceId, committeeMemberId).flatMapMany(isRegister -> {
            if (isRegister) {
                return conferenceSectionDAO.findSectionsForCommitteeMember(committeeMemberId)
                                           .filterWhen(section -> isSectionBelongsToConference(conferenceId, section.getId()))
                                           .flatMap(section -> applyDAO.findByConferenceSection(conferenceId, section.getId()))
                                           .filterWhen(this::filterApplicationsForReview);
            } else {
                return Mono.error(new Exception("User is not part of the conference"));
            }
        });
    }

    private Mono<Boolean> isSectionBelongsToConference(int conferenceId, int sectionId) {
        return conferenceSectionDAO.isSectionBelongsToConference(conferenceId, sectionId).map(count -> count.equals(1));
    }

    private Mono<Boolean> filterApplicationsForReview(ApplyModel applicationModel) {
        return Mono.just(applicationModel)
                   .flatMap(application -> {
                       if (application.getStatus().equals(ApplyStatusEnum.OPENED) || application.getStatus()
                                                                                                .equals(ApplyStatusEnum.IN_REVIEW)) {
                           return reviewDAO.findByApply(application.getId())
                                           .count()
                                           .map(count -> count < 2);
                       } else {
                           return Mono.just(false);
                       }
                   });
    }

    private ApplyModel createApplyModel(DocumentModel document) {
        ApplyModel applyModel = new ApplyModel();
        applyModel.setDocument(document);
        applyModel.setConferenceMember(document.getUser());
        applyModel.setStatus(ApplyStatusEnum.OPENED);
        return applyModel;
    }
}
