package com.fon.confms.service.impl;

import com.fon.confms.dao.CountryDAO;
import com.fon.confms.model.CountryModel;
import com.fon.confms.service.CountryService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class CountryServiceImpl implements CountryService {

    @Resource
    private CountryDAO countryDAO;

    @Override
    public Flux<CountryModel> getAll() {
        return countryDAO.findAll();
    }

    @Override
    public Mono<CountryModel> getByID(int id) {
        return countryDAO.findById(id);
    }
}
