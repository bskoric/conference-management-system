package com.fon.confms.service.impl;

import com.fon.confms.dao.DocumentCategoryDAO;
import com.fon.confms.dao.DocumentDAO;
import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.service.DocumentService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Resource
    private DocumentDAO documentDAO;

    @Resource
    private DocumentCategoryDAO documentCategoryDAO;

    @Override
    public Flux<DocumentModel> getAll() {
        return documentDAO.findAll();
    }

    @Override
    public Mono<DocumentModel> getByID(int id) {
        return documentDAO.findById(id);
    }

    @Override
    public Flux<DocumentCategoryModel> getCategories() {
        return documentCategoryDAO.findAll();
    }

    @Override
    public Mono<DocumentCategoryModel> getCategory(int categoryId) {
        return documentCategoryDAO.findById(categoryId);
    }

    @Override
    public Mono<DocumentModel> createDocument(DocumentModel document) {
        return documentDAO.save(document);
    }
}
