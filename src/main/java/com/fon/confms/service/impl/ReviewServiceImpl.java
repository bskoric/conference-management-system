package com.fon.confms.service.impl;

import com.fon.confms.dao.ApplyDAO;
import com.fon.confms.dao.ReviewDAO;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import com.fon.confms.service.ReviewService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Resource
    private ReviewDAO reviewDAO;

    @Resource
    private ApplyDAO applyDAO;

    @Override
    public Flux<ReviewModel> getAll() {
        return reviewDAO.findAll();
    }

    @Override
    public Mono<ReviewModel> getByID(int id) {
        return reviewDAO.findById(id);
    }

    @Override
    public Mono<ReviewModel> getReview(int committeeMemberId, int applyID) {
        return reviewDAO.findByCommitteeMemberIdAndApply(committeeMemberId, applyID);
    }

    @Override
    public Flux<ReviewModel> getReviews(int conferenceMember, int conferenceId) {
        return reviewDAO.findByConferenceMember(conferenceMember, conferenceId);
    }

    @Override
    public Flux<ReviewModel> getReviews(int committeeMember, int conferenceId, String applyStatus) {
        return reviewDAO.findByCommitteeMemberIdAndApplyStatus(committeeMember, conferenceId, applyStatus);
    }

    @Override
    public Flux<ReviewModel> getReviews(int applyID) {
        return reviewDAO.findByApply(applyID);
    }

    @Override
    public Mono<Void> changeStatus(int reviewId, ReviewStatusEnum status) {
        return reviewDAO.updateStatus(status.toString(), reviewId);
    }

    @Override
    public Mono<ReviewModel> createReview(ApplyModel application, UserModel committeeMember) {
        ReviewModel reviewModel = new ReviewModel();
        reviewModel.setApply(application);
        reviewModel.setCommitteeMember(committeeMember);
        reviewModel.setStatus(ReviewStatusEnum.IN_PROGRESS);
        return reviewDAO.save(reviewModel)
                        .onErrorResume(e -> {
                            if (e instanceof DataIntegrityViolationException) {
                                return Mono.error(new DataIntegrityViolationException(
                                    "User is already reviewer"));
                            } else {
                                return Mono.error(e);
                            }
                        })
                        .doOnNext(review -> applyDAO.updateStatus(ApplyStatusEnum.IN_REVIEW.toString(),
                                                                  application.getId()).subscribe());
    }
}
