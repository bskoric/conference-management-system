package com.fon.confms.service.impl;

import com.fon.confms.dao.RoleDAO;
import com.fon.confms.model.RoleModel;
import com.fon.confms.service.RoleService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class RoleServiceImpl implements RoleService {

    public static final String ROLE_USER = "USER";
    public static final String ROLE_COMMITTEE_MEMBER = "COMMITTEE_MEMBER";

    @Resource
    private RoleDAO roleDAO;

    @Override
    public Flux<RoleModel> getAll() {
        return roleDAO.findAll();
    }

    @Override
    public Mono<RoleModel> getByID(int id) {
        return roleDAO.findById(id);
    }

    @Override
    public Mono<RoleModel> getByName(String roleName) {
        return roleDAO.findByName(roleName);
    }
}
