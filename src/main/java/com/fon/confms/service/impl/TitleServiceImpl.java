package com.fon.confms.service.impl;

import com.fon.confms.dao.TitleDAO;
import com.fon.confms.model.TitleModel;
import com.fon.confms.service.TitleService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class TitleServiceImpl implements TitleService {

    @Resource
    private TitleDAO titleDAO;

    @Override
    public Flux<TitleModel> getAll() {
        return titleDAO.findAll();
    }

    @Override
    public Mono<TitleModel> getByID(int id) {
        return titleDAO.findById(id);
    }
}
