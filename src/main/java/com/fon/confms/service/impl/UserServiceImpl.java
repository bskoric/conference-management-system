package com.fon.confms.service.impl;

import com.fon.confms.dao.UserDAO;
import com.fon.confms.model.UserModel;
import com.fon.confms.service.RoleService;
import com.fon.confms.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Base64;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDAO userDAO;

    @Resource
    private RoleService roleService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public Flux<UserModel> getAll() {
        return userDAO.findAll();
    }

    @Override
    public Mono<UserModel> getByID(int id) {
        return userDAO.findById(id);
    }

    @Override
    public Mono<UserModel> getByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    @Override
    public Mono<String> getUserConferenceStatus(int conferenceId, int userId) {
        return userDAO.findUserConferenceStatus(conferenceId, userId);
    }

    @Override
    public Mono<UserModel> register(UserModel user) {
        return roleService.getByName(RoleServiceImpl.ROLE_USER).flatMap(role -> {
            user.setRole(role);
            user.setPassword(passwordEncoder.encode(new String(Base64.getDecoder().decode(user.getPassword()))));
            return userDAO.save(user);
        });
    }
}
