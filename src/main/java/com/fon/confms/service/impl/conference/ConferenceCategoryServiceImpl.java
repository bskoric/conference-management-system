package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceCategoryDAO;
import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.service.conference.ConferenceCategoryService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class ConferenceCategoryServiceImpl implements ConferenceCategoryService {

    @Resource
    private ConferenceCategoryDAO conferenceCategoryDAO;

    @Override
    public Flux<ConferenceCategoryModel> getAll() {
        return conferenceCategoryDAO.findAll();
    }

    @Override
    public Mono<ConferenceCategoryModel> getByID(int id) {
        return conferenceCategoryDAO.findById(id);
    }
}
