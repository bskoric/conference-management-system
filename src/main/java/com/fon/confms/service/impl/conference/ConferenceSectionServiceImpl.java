package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceSectionDAO;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.service.conference.ConferenceSectionService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class ConferenceSectionServiceImpl implements ConferenceSectionService {

    @Resource
    private ConferenceSectionDAO conferenceSectionDAO;

    @Override
    public Flux<ConferenceSectionModel> getAll() {
        return conferenceSectionDAO.findAll();
    }

    @Override
    public Mono<ConferenceSectionModel> getByID(int id) {
        return conferenceSectionDAO.findById(id);
    }

    @Override
    public Flux<ConferenceSectionModel> getSectionsForConference(int conferenceId) {
        return conferenceSectionDAO.findSectionsForConference(conferenceId);
    }

    @Override
    public Flux<ConferenceSectionModel> getSectionsForCategory(int categoryId) {
        return conferenceSectionDAO.findSectionsForCategory(categoryId);
    }

    @Override
    public Flux<ConferenceSectionModel> getSectionsForCommitteeMember(int committeeMemberId) {
        return conferenceSectionDAO.findSectionsForCommitteeMember(committeeMemberId);
    }
}
