package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceDAO;
import com.fon.confms.dao.UserDAO;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.UserStatusEnum;
import com.fon.confms.service.conference.ConferenceService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    @Resource
    private ConferenceDAO conferenceDAO;

    @Resource
    private UserDAO userDAO;

    @Override
    public Flux<ConferenceModel> getAll() {
        return conferenceDAO.findAll();
    }

    @Override
    public Mono<ConferenceModel> getByID(int id) {
        return conferenceDAO.findById(id);
    }

    @Override
    public Flux<UserModel> getConferenceMembers(int conferenceId) {
        return userDAO.findConferenceUserByConference(conferenceId);
    }

    @Override
    public Flux<UserModel> getCommitteeMembers(int conferenceId) {
        return userDAO.findCommitteeMemberUserByConference(conferenceId);
    }


    @Override
    public Mono<Boolean> isUserRegisterToConferenceApp(int conferenceId, int userId) {
        return conferenceDAO.isUserRegisterToConferenceApp(conferenceId, userId)
                            .map(count -> count.equals(1));
    }

    @Override
    public Mono<Boolean> registerToConferenceApp(int conferenceId, int userId) {
        return conferenceDAO.registerToConference(conferenceId, userId, UserStatusEnum.PENDING_APP_REGISTRATION.toString())
                            .map(count -> count.equals(1));
    }

    @Override
    public Flux<ConferenceModel> findConferencesForUser(UserModel user) {
        return null;
    }
}
