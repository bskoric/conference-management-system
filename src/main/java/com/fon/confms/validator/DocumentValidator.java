package com.fon.confms.validator;

import com.fon.confms.model.DocumentModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class DocumentValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        DocumentModel document = (DocumentModel) target;

        if (StringUtils.isBlank(document.getName())) {
            errors.rejectValue("name", "name.empty", "Name is empty");
        }

        if (StringUtils.isBlank(document.getDescription())) {
            errors.rejectValue("description", "description.empty", "Description is empty");
        }

        if (Objects.isNull(document.getUser())) {
            errors.rejectValue("user", "user.empty", "System error: User is empty.");
        }

        if (Objects.isNull(document.getConference())) {
            errors.rejectValue("conference", "conference.empty", "System error: conference is empty.");
        }
    }
}
