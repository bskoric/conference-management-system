package com.fon.confms.controller;

import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.model.RoleModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.service.ApplyService;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;

class ApplyControllerTest extends BaseTest {

    private static final String APPLICATIONS_BASE = "/api/applications";
    private static final String APPLICATIONS = "/api/applications/conferences/1/users/1";
    private static final String APPLICATION = "/api/applications/1";
    private static final String APPLICATION2 = "/api/applications/1/conferences/1";
    private static final String APPLICATIONS_FOR_REVIEW = "/api/applications/conferences/1/committee-member/3";
    private static final String APPLICATION_COMMENTS = "/api/applications/1/comments";
    private static final String APPLICATION_COMMENT_DELETE = "/api/applications/comments/1/users/1";
    private static final String APPLICATION_CLOSE = "/api/applications/1/close";
    private static final String APPLICATION_DELETE = "/api/applications/1/delete";
    private static final String APPLICATION_FINISH = "/api/applications/1/finish";

    @MockBean
    private ApplyService applyService;

    private ApplyModel applicationModel;
    private ApplyModel applicationModel2;
    private ConferenceModel conferenceModel;
    private ApplicationCommentModel applicationCommentModel;
    private DocumentModel documentModel;

    @BeforeEach
    void setup() {
        RoleModel role = RoleModel.builder().id(1).name("ROLE_USER").build();
        UserModel userModel = UserModel.builder().id(1).name("User 1").surname("Surname 1").role(role).build();
        UserModel userModel2 = UserModel.builder().id(2).name("User 2").surname("Surname 2").role(role).build();

        conferenceModel = ConferenceModel.builder().id(1).name("Test conference 1").town("Belgrade").isClosed(false).build();
        byte[] file = new byte[10];

        documentModel = DocumentModel.builder()
                                     .id(10)
                                     .name("Test document")
                                     .description("Description")
                                     .fileName("Test file")
                                     .file(file)
                                     .user(userModel)
                                     .build();

        DocumentModel documentModel2 = DocumentModel.builder()
                                                    .id(10)
                                                    .name("Test document1")
                                                    .description("Description")
                                                    .fileName("Test file")
                                                    .file(file)
                                                    .user(userModel2)
                                                    .build();

        applicationModel =
            ApplyModel.builder().id(1).conferenceMember(userModel).document(documentModel).status(ApplyStatusEnum.IN_REVIEW).build();
        applicationModel2 =
            ApplyModel.builder().id(2).conferenceMember(userModel2).document(documentModel2).status(ApplyStatusEnum.IN_REVIEW).build();

        applicationCommentModel = ApplicationCommentModel.builder().id(1).text("Text 1").apply(applicationModel).user(userModel).build();
    }

    @Test
    void testGetApplication() {
        when(applyService.getByID(1)).thenReturn(Mono.just(applicationModel));

        getWebTestClientRequest(webTestClient, APPLICATION)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ApplyModel.class).isEqualTo(applicationModel);
    }

    @Test
    void testGetApplication2() {
        when(applyService.getApply(1, conferenceModel.getId())).thenReturn(Mono.just(applicationModel));

        getWebTestClientRequest(webTestClient, APPLICATION2)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ApplyModel.class).isEqualTo(applicationModel);
    }

    @Test
    void testGetApplications() {
        when(applyService.getByUserAndConference(conferenceModel.getId(), 1)).thenReturn(Flux.just(applicationModel, applicationModel2));

        getWebTestClientRequest(webTestClient, APPLICATIONS)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ApplyModel.class).hasSize(2).contains(applicationModel, applicationModel2);
    }

    @Test
    void testGetApplicationsForReview() {
        when(applyService.getApplicationsForCommitteeMember(conferenceModel.getId(), 3))
            .thenReturn(Flux.just(applicationModel, applicationModel2));

        getWebTestClientRequest(webTestClient, APPLICATIONS_FOR_REVIEW)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.body[0].conferenceMember.id").isEqualTo(1)
            .jsonPath("$.body[0].document.name").isEqualTo("Test document")
            .jsonPath("$.body[1].document.name").isEqualTo("Test document1")
            .jsonPath("$.body[1].status").isEqualTo(ApplyStatusEnum.IN_REVIEW.toString())
            .jsonPath("$.errorMessage").isEqualTo(StringUtils.EMPTY);

    }

    @Test
    void testGetApplicationsForReviewError() {
        when(applyService.getApplicationsForCommitteeMember(conferenceModel.getId(), 3))
            .thenReturn(Flux.error(new Exception("User is not part of the conference")));

        getWebTestClientRequest(webTestClient, APPLICATIONS_FOR_REVIEW)
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.errorMessage").isEqualTo("User is not part of the conference");

    }

    @Test
    void testGetCommentsForApplication() {
        when(applyService.getCommentsForApplication(applicationModel.getId())).thenReturn(Flux.just(applicationCommentModel));

        getWebTestClientRequest(webTestClient, APPLICATION_COMMENTS)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ApplicationCommentModel.class).hasSize(1).contains(applicationCommentModel);

    }

    @Test
    void testAddCommentsForApplication() {
        when(applyService.createComment(applicationCommentModel)).thenReturn(Mono.just(applicationCommentModel));

        postWebTestClientRequest(webTestClient, APPLICATION_COMMENTS)
            .body(Mono.just(applicationCommentModel), ApplicationCommentModel.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ApplicationCommentModel.class).isEqualTo(applicationCommentModel);
    }

    @Test
    void testDeleteComment() {
        when(applyService.deleteComment(1, 1)).thenReturn(Mono.empty().then());

        webTestClient.delete()
                     .uri(APPLICATION_COMMENT_DELETE)
                     .accept(MediaType.APPLICATION_JSON)
                     .headers(http -> http.setBearerAuth(roleUserToken))
                     .exchange()
                     .expectStatus().isOk();
    }

    @Test
    void testCloseApplication() {
        when(applyService.closeApplication(applicationModel.getId())).thenReturn(Mono.empty().then());

        postWebTestClientRequest(webTestClient, APPLICATION_CLOSE)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    void testDeleteApp() {
        when(applyService.deleteApplication(applicationModel.getId())).thenReturn(Mono.empty().then());

        webTestClient.delete()
                     .uri(APPLICATION_DELETE)
                     .accept(MediaType.APPLICATION_JSON)
                     .headers(http -> http.setBearerAuth(roleUserToken))
                     .exchange()
                     .expectStatus().isOk();
    }

    @Test
    void testFinishApplication() {
        when(applyService.finishApplication(applicationModel.getId())).thenReturn(Mono.empty().then());

        postWebTestClientRequest(webTestClient, APPLICATION_FINISH)
            .body(Mono.just(applicationCommentModel), ApplicationCommentModel.class)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    void testFinishApplicationError() {
        when(applyService.finishApplication(applicationModel.getId())).thenReturn(Mono.error(new Exception("You must have 2 approvals")));

        postWebTestClientRequest(webTestClient, APPLICATION_FINISH)
            .body(Mono.just(applicationCommentModel), ApplicationCommentModel.class)
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.errorMessage").isEqualTo("You must have 2 approvals");
    }

    @Test
    void testCreateApply() {
        String jsonDocument = "{\n"
            + "\t\"id\": 10,\n"
            + "\t\"name\": \"Test document1\",\n"
            + "\t\"description\": \"Description\",\n"
            + "\t\"createdAt\": null,\n"
            + "\t\"file\": \"AAAAAAAAAAAAAA==\",\n"
            + "\t\"fileName\": \"Test file\",\n"
            + "\t\"user\": {\n"
            + "\t\t\"id\": 0,\n"
            + "\t\t\"name\": \"User 2\",\n"
            + "\t\t\"surname\": \"Surname 2\",\n"
            + "\t\t\"email\": null,\n"
            + "\t\t\"username\": null,\n"
            + "\t\t\"title\": null,\n"
            + "\t\t\"country\": null,\n"
            + "\t\t\"role\": {\n"
            + "\t\t\t\"id\": 1,\n"
            + "\t\t\t\"name\": \"ROLE_USER\"\n"
            + "\t\t}\n"
            + "\t}\n"
            + "}";

        MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("file", new ClassPathResource("/images/CONFMS.png")).contentType(MediaType.MULTIPART_FORM_DATA);
        multipartBodyBuilder.part("document", jsonDocument).contentType(MediaType.MULTIPART_FORM_DATA);

        postWebTestClientRequest(webTestClient, APPLICATIONS_BASE)
            .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
            .exchange()
            .expectStatus().isOk();
    }
}