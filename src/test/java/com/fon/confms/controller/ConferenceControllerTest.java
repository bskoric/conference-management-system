package com.fon.confms.controller;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.model.RoleModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.UserStatusEnum;
import com.fon.confms.service.UserService;
import com.fon.confms.service.conference.ConferenceCategoryService;
import com.fon.confms.service.conference.ConferenceSectionService;
import com.fon.confms.service.conference.ConferenceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;


class ConferenceControllerTest extends BaseTest {

    private static final String CONFERENCES = "/api/conferences";
    private static final String CONFERENCE = "/api/conferences/1";
    private static final String CONFERENCE_USERS = "/api/conferences/1/users";
    private static final String CONFERENCE_USER_STATUS = "/api/conferences/1/users/1/is-member";
    private static final String CONFERENCE_REGISTER = "/api/conferences/1/users/1/register";
    private static final String CONFERENCE_SECTIONS = "/api/conferences/categories/1";


    @MockBean
    private ConferenceService conferenceService;

    @MockBean
    private UserService userService;

    @MockBean
    private ConferenceCategoryService conferenceCategoryService;

    @MockBean
    private ConferenceSectionService conferenceSectionService;

    private ConferenceModel conferenceModel;
    private ConferenceModel conferenceModel2;

    @BeforeEach
    void setup() {
        conferenceModel = ConferenceModel.builder().id(1).name("Test conference 1").town("Belgrade").isClosed(false).build();
        conferenceModel2 = ConferenceModel.builder().id(2).name("Test conference 2").town("Belgrade").isClosed(false).build();
    }

    @Test
    void testGetConference() {
        when(conferenceService.getByID(1)).thenReturn(Mono.just(conferenceModel));

        getWebTestClientRequest(webTestClient, CONFERENCE)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ConferenceModel.class).isEqualTo(conferenceModel);
    }

    @Test
    void testGetConferenceNotFound() {
        when(conferenceService.getByID(1)).thenReturn(Mono.empty());

        getWebTestClientRequest(webTestClient, CONFERENCE)
            .exchange()
            .expectStatus().isNotFound();
    }

    @Test
    void testGetConferences() {
        when(conferenceService.getAll()).thenReturn(Flux.just(conferenceModel, conferenceModel2));

        getWebTestClientRequest(webTestClient, CONFERENCES)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ConferenceModel.class).contains(conferenceModel, conferenceModel2).hasSize(2);
    }

    @Test
    void testGetConferenceMembers() {
        RoleModel role = RoleModel.builder().id(1).name("ROLE_USER").build();
        UserModel userModel = UserModel.builder().name("User 1").surname("Surname 1").role(role).build();
        UserModel userModel2 = UserModel.builder().name("User 2").surname("Surname 2").role(role).build();

        when(conferenceService.getConferenceMembers(1)).thenReturn(Flux.just(userModel, userModel2));

        getWebTestClientRequest(webTestClient, CONFERENCE_USERS)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(UserModel.class).contains(userModel, userModel2).hasSize(2);
    }

    @Test
    void testGetUserConferenceStatus() {
        when(conferenceService.isUserRegisterToConferenceApp(1, 1)).thenReturn(Mono.just(true));
        when(userService.getUserConferenceStatus(1, 1)).thenReturn(Mono.just(UserStatusEnum.CONFERENCE_APPROVED.toString()));

        getWebTestClientRequest(webTestClient, CONFERENCE_USER_STATUS)
            .exchange()
            .expectStatus().isOk()
            .expectBody().json("{\"status\":\"CONFERENCE_APPROVED\",\"userRegisterToConferenceApp\":true}");
    }

    @Test
    void testGetUserConferenceStatusNotRegister() {
        when(conferenceService.isUserRegisterToConferenceApp(1, 1)).thenReturn(Mono.just(false));

        getWebTestClientRequest(webTestClient, CONFERENCE_USER_STATUS)
            .exchange()
            .expectStatus().isOk()
            .expectBody().json("{\"userRegisterToConferenceApp\":false}");
    }

    @Test
    void testRegisterToConference() {
        when(conferenceService.registerToConferenceApp(1, 1)).thenReturn(Mono.just(true));

        webTestClient.post()
                     .uri(CONFERENCE_REGISTER)
                     .accept(MediaType.APPLICATION_JSON)
                     .headers(http -> http.setBearerAuth(roleUserToken))
                     .exchange()
                     .expectStatus().isOk()
                     .expectBody(Boolean.class).isEqualTo(true);
    }

    @Test
    void testRegisterToConferenceForbidden() {
        postWebTestClientRequest(webTestClient, CONFERENCE_REGISTER)
            .exchange()
            .expectStatus().isForbidden();
    }

    @Test
    void testRegisterToConferenceWrongMethod() {
        getWebTestClientRequest(webTestClient, CONFERENCE_REGISTER)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    void testGetSections() {
        ConferenceCategoryModel conferenceCategory = ConferenceCategoryModel.builder().id(1).name("IT").description("Description").build();
        ConferenceSectionModel conferenceSectionModel =
            ConferenceSectionModel.builder().id(1).name("Section 1").category(conferenceCategory).build();
        ConferenceSectionModel conferenceSectionModel2 =
            ConferenceSectionModel.builder().id(2).name("Section 2").category(conferenceCategory).build();

        when(conferenceSectionService.getSectionsForConference(1)).thenReturn(Flux.just(conferenceSectionModel, conferenceSectionModel2));

        getWebTestClientRequest(webTestClient, CONFERENCE_SECTIONS)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ConferenceSectionModel.class).contains(conferenceSectionModel, conferenceSectionModel2).hasSize(2);
    }

    @Test
    void testGetConferenceCategory() {
        ConferenceCategoryModel conferenceCategory = ConferenceCategoryModel.builder().id(1).name("IT").description("Description").build();
        when(conferenceCategoryService.getByID(1)).thenReturn(Mono.just(conferenceCategory));

        getWebTestClientRequest(webTestClient, CONFERENCE_SECTIONS)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ConferenceCategoryModel.class).isEqualTo(conferenceCategory);
    }
}