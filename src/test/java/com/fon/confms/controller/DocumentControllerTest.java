package com.fon.confms.controller;

import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.service.DocumentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.CacheControl;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;


class DocumentControllerTest extends BaseTest {

    private static final String DOCUMENT = "/api/documents/1";
    private static final String DOCUMENTS_FILE = "/api/documents/1/file";
    private static final String DOCUMENTS_CATEGORIES = "/api/documents/categories";

    @MockBean
    private DocumentService documentService;

    private DocumentModel documentModel;
    private byte[] file;

    @BeforeEach
    void setup() {
        file = new byte[10];
        documentModel = DocumentModel.builder()
                                     .id(10)
                                     .name("Test document")
                                     .description("Description")
                                     .fileName("Test file")
                                     .file(file)
                                     .build();
    }

    @Test
    void testGeDocument() {
        when(documentService.getByID(1)).thenReturn(Mono.just(documentModel));

        getWebTestClientRequest(webTestClient, DOCUMENT)
            .exchange()
            .expectStatus().isOk()
            .expectBody(DocumentModel.class).isEqualTo(documentModel);
    }

    @Test
    void testGetDocumentFileForDownload() {
        when(documentService.getByID(1)).thenReturn(Mono.just(documentModel));

        getWebTestClientRequest(webTestClient, DOCUMENTS_FILE)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentDisposition(ContentDisposition.attachment().filename("Test file").build())
            .expectHeader().contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE)
            .expectHeader().cacheControl(CacheControl.noCache())
            .expectBody(byte[].class).isEqualTo(file);
    }

    @Test
    void testGeDocumentCategories() {
        DocumentCategoryModel documentCategoryModel = DocumentCategoryModel.builder().id(1).name("Scientific work").build();
        DocumentCategoryModel documentCategoryModel2 = DocumentCategoryModel.builder().id(2).name("Master thesis").build();

        when(documentService.getCategories()).thenReturn(Flux.just(documentCategoryModel, documentCategoryModel2));

        getWebTestClientRequest(webTestClient, DOCUMENTS_CATEGORIES)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(DocumentCategoryModel.class).hasSize(2).contains(documentCategoryModel, documentCategoryModel2);
    }
}