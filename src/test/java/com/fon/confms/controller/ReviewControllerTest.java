package com.fon.confms.controller;

import com.fon.confms.dto.request.CreateReviewRequestDTO;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import com.fon.confms.service.ReviewService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class ReviewControllerTest extends BaseTest {

    private static final String REVIEW_BASE = "/api/reviews";
    private static final String REVIEWS_CONF_USER = REVIEW_BASE + "/conferences/2/users/2";
    private static final String REVIEWS_DECLINED = REVIEW_BASE + "/1/status?type=DECLINED";

    @MockBean
    private ReviewService reviewService;

    private ReviewModel reviewModel;
    private UserModel committeeUserModel;
    private ApplyModel applicationModel;

    @BeforeEach
    void setup() {
        UserModel userModel = UserModel.builder().id(1).username("userUsername").build();
        committeeUserModel = UserModel.builder().id(2).username("cmUsername").build();
        applicationModel = ApplyModel.builder().id(1).conferenceMember(userModel).status(ApplyStatusEnum.IN_REVIEW).build();
        reviewModel = ReviewModel.builder()
                                 .id(1)
                                 .apply(applicationModel)
                                 .committeeMember(committeeUserModel)
                                 .status(ReviewStatusEnum.APPROVED)
                                 .build();
    }

    @Test
    void testGerReviews() {
        Flux<ReviewModel> reviewsFlux = Flux.just(reviewModel);
        when(reviewService.getReviews(2, 2)).thenReturn(reviewsFlux);

        getWebTestClientRequest(webTestClient, REVIEWS_CONF_USER)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ReviewModel.class)
            .value(response -> assertThat(response.get(0).getCommitteeMember().getUsername()).isEqualTo("cmUsername"))
            .contains(reviewModel).hasSize(1);

    }

    @Test
    void testGerReviewsByApplication() {
        Flux<ReviewModel> reviewsFlux = Flux.just(reviewModel);
        when(reviewService.getReviews(1)).thenReturn(reviewsFlux);

        getWebTestClientRequest(webTestClient, REVIEW_BASE + "/applications/1")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ReviewModel.class)
            .value(response -> assertThat(response.get(0).getCommitteeMember().getUsername()).isEqualTo("cmUsername"))
            .contains(reviewModel).hasSize(1);

    }

    @Test
    void testChangeReviewStatus() {
        when(reviewService.changeStatus(1, ReviewStatusEnum.DECLINED)).thenReturn(Mono.empty().then());

        postWebTestClientRequest(webTestClient, REVIEWS_DECLINED)
            .exchange()
            .expectStatus().isOk();
    }


    @Test
    void testChangeReviewStatusForbidden() {
        webTestClient.post()
                     .uri(REVIEWS_DECLINED)
                     .accept(MediaType.APPLICATION_JSON)
                     .headers(http -> http.setBearerAuth(roleUserToken))
                     .exchange()
                     .expectStatus().isForbidden();
    }

    @Test
    void testCreateReview() {
        CreateReviewRequestDTO review = new CreateReviewRequestDTO();
        review.setApplication(applicationModel);
        review.setCommitteeMember(committeeUserModel);

        ReviewModel createdReview = ReviewModel.builder()
                                               .id(23)
                                               .apply(applicationModel)
                                               .committeeMember(committeeUserModel)
                                               .status(ReviewStatusEnum.IN_PROGRESS)
                                               .build();

        when(reviewService.createReview(review.getApplication(), review.getCommitteeMember())).thenReturn(
            Mono.just(createdReview));

        postWebTestClientRequest(webTestClient, REVIEW_BASE)
            .body(Mono.just(review), CreateReviewRequestDTO.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.body.committeeMember.id").isEqualTo(2)
            .jsonPath("$.body.apply.conferenceMember.username").isEqualTo("userUsername")
            .jsonPath("$.body.status").isEqualTo(ReviewStatusEnum.IN_PROGRESS.toString());
    }

    @Test
    void testCreateReviewForbidden() {
        CreateReviewRequestDTO review = new CreateReviewRequestDTO();

        webTestClient.post()
                     .uri(REVIEW_BASE)
                     .accept(MediaType.APPLICATION_JSON)
                     .headers(http -> http.setBearerAuth(roleUserToken))
                     .body(Mono.just(review), CreateReviewRequestDTO.class)
                     .exchange()
                     .expectStatus().isForbidden();
    }

    @Test
    void testCreateReviewError() {
        CreateReviewRequestDTO review = new CreateReviewRequestDTO();
        review.setApplication(applicationModel);
        review.setCommitteeMember(committeeUserModel);

        when(reviewService.createReview(review.getApplication(), review.getCommitteeMember())).thenReturn(
            Mono.error(new DataIntegrityViolationException("User is already reviewer")));

        postWebTestClientRequest(webTestClient, REVIEW_BASE)
            .body(Mono.just(review), CreateReviewRequestDTO.class)
            .exchange()
            .expectStatus()
            .is4xxClientError()
            .expectBody()
            .jsonPath("$.errorMessage").isEqualTo("User is already reviewer");
    }
}