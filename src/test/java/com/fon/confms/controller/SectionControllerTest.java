package com.fon.confms.controller;

import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.service.conference.ConferenceSectionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class SectionControllerTest extends BaseTest {

    private static final String SECTION_BASE = "/api/sections";
    private static final String SECTION_1 = SECTION_BASE + "/1";
    private static final String CATEGORY_IT = "IT";

    @MockBean
    private ConferenceSectionService conferenceSectionService;

    private ConferenceSectionModel conferenceSection;
    private ConferenceSectionModel conferenceSection2;

    @BeforeEach
    void setup() {
        ConferenceCategoryModel conferenceCategory = ConferenceCategoryModel.builder().id(1).name(CATEGORY_IT).description("Description").build();
        conferenceSection = ConferenceSectionModel.builder().id(1).name("Database section").category(conferenceCategory).build();
        conferenceSection2 = ConferenceSectionModel.builder().id(2).name("Software development").category(conferenceCategory).build();
    }

    @Test
    void testGetSection() {
        Mono<ConferenceSectionModel> conferenceSectionModelMono = Mono.just(conferenceSection);

        when(conferenceSectionService.getByID(1)).thenReturn(conferenceSectionModelMono);

        getWebTestClientRequest(webTestClient, SECTION_1)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ConferenceSectionModel.class)
            .value(response -> assertThat(response.getName()).isEqualTo("Database section"));
    }

    @Test
    void testGetSectionRealDB() throws IOException {
        getWebTestClientRequest(getWebTestClientBindServer(), SECTION_1)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .json(getJson("json/section/section1.json"));
    }

    @Test
    void testGetSectionsOfCommitteeMember() {
        Flux<ConferenceSectionModel> conferenceSectionFlux = Flux.just(conferenceSection, conferenceSection2);

        when(conferenceSectionService.getSectionsForCommitteeMember(1)).thenReturn(conferenceSectionFlux);

        getWebTestClientRequest(webTestClient, SECTION_BASE + "/committee-member/1")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ConferenceSectionModel.class)
            .value(list -> assertThat(list.get(1).getName()).isEqualTo(conferenceSection2.getName()));
    }

    @Test
    void testGetSectionsOfCategory() {
        Flux<ConferenceSectionModel> conferenceSectionFlux = Flux.just(conferenceSection, conferenceSection2);

        when(conferenceSectionService.getSectionsForCategory(1)).thenReturn(conferenceSectionFlux);

        getWebTestClientRequest(webTestClient, SECTION_BASE + "/categories/1")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(ConferenceSectionModel.class)
            .value(list -> assertThat(list.get(1).getName()).isEqualTo(conferenceSection2.getName()))
            .value(list -> assertThat(list.get(0).getCategory().getName()).isEqualTo(CATEGORY_IT))
            .value(list -> assertThat(list.get(1).getCategory().getName()).isEqualTo(CATEGORY_IT))
            .hasSize(2);
    }
}