package com.fon.confms.service.impl;

import com.fon.confms.dao.ApplyCommentDAO;
import com.fon.confms.dao.ApplyDAO;
import com.fon.confms.dao.ConferenceSectionDAO;
import com.fon.confms.dao.ReviewDAO;
import com.fon.confms.model.ApplicationCommentModel;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.ConferenceSectionModel;
import com.fon.confms.model.DocumentModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.RoleModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import com.fon.confms.service.DocumentService;
import com.fon.confms.service.conference.ConferenceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class ApplyServiceImplTest {

    @InjectMocks
    private ApplyServiceImpl applyService;

    @Mock
    private ApplyDAO applyDAO;

    @Mock
    private DocumentService documentService;

    @Mock
    private ApplyCommentDAO applyCommentDAO;

    @Mock
    private ReviewDAO reviewDAO;

    @Mock
    private ConferenceSectionDAO conferenceSectionDAO;

    @Mock
    private ConferenceService conferenceService;

    private DocumentModel documentModel;
    private ApplyModel applicationModel;
    private ApplyModel applicationModel2;
    private ApplicationCommentModel applicationCommentModel;
    private ReviewModel reviewModel;
    private ReviewModel reviewModel2;
    private ConferenceSectionModel conferenceSection;
    private ConferenceSectionModel conferenceSection2;

    @BeforeEach
    void setUp() {
        byte[] file = new byte[10];

        RoleModel role = RoleModel.builder().id(1).name("ROLE_USER").build();
        UserModel userModel = UserModel.builder().id(1).name("User 1").surname("Surname 1").role(role).build();
        UserModel userModel2 = UserModel.builder().id(2).name("User 2").surname("Surname 2").role(role).build();

        documentModel = DocumentModel.builder()
                                     .id(10)
                                     .name("Test document")
                                     .description("Description")
                                     .fileName("Test file")
                                     .file(file)
                                     .conference(ConferenceModel.builder().id(1).build())
                                     .user(userModel)
                                     .build();

        DocumentModel documentModel2 = DocumentModel.builder()
                                                    .id(10)
                                                    .name("Test document1")
                                                    .description("Description")
                                                    .fileName("Test file")
                                                    .file(file)
                                                    .user(userModel2)
                                                    .build();

        UserModel committeeUserModel = UserModel.builder().id(2).username("cmUsername").build();
        UserModel committeeUserModel2 = UserModel.builder().id(23).username("cmUsername23").build();
        applicationModel =
            ApplyModel.builder().id(1).conferenceMember(userModel).document(documentModel).status(ApplyStatusEnum.IN_REVIEW).build();
        applicationModel2 =
            ApplyModel.builder().id(2).conferenceMember(userModel2).document(documentModel2).status(ApplyStatusEnum.IN_REVIEW).build();
        applicationCommentModel = ApplicationCommentModel.builder().id(1).text("Text 1").apply(applicationModel).user(userModel).build();
        reviewModel = ReviewModel.builder()
                                 .id(1)
                                 .apply(applicationModel)
                                 .committeeMember(committeeUserModel)
                                 .status(ReviewStatusEnum.APPROVED)
                                 .build();
        reviewModel2 = ReviewModel.builder()
                                  .id(1)
                                  .apply(applicationModel)
                                  .committeeMember(committeeUserModel2)
                                  .status(ReviewStatusEnum.IN_PROGRESS)
                                  .build();

        ConferenceCategoryModel
            conferenceCategory = ConferenceCategoryModel.builder().id(1).name("IT").description("Description").build();
        conferenceSection = ConferenceSectionModel.builder().id(1).name("Database section").category(conferenceCategory).build();
        conferenceSection2 = ConferenceSectionModel.builder().id(2).name("Software development").category(conferenceCategory).build();
    }

    @Test
    void testCreateApply() {
        when(documentService.createDocument(documentModel)).thenReturn(Mono.just(documentModel));
        when(applyDAO.save(any())).thenReturn(Mono.just(applicationModel));

        StepVerifier.create(applyService.createApply(documentModel))
                    .expectSubscription()
                    .expectNext(applicationModel)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testActiveApplyNotExist() {
        applicationModel.setStatus(ApplyStatusEnum.DECLINED);
        applicationModel2.setStatus(ApplyStatusEnum.DECLINED);
        when(applyDAO.findByUserAndConference(1, 1)).thenReturn(Flux.just(applicationModel, applicationModel2));

        StepVerifier.create(applyService.activeApplyNotExist(documentModel))
                    .expectSubscription()
                    .expectNext(true)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testActiveApplyNotExistFalse() {
        when(applyDAO.findByUserAndConference(1, 1)).thenReturn(Flux.just(applicationModel, applicationModel2));

        StepVerifier.create(applyService.activeApplyNotExist(documentModel))
                    .expectSubscription()
                    .expectNext(false)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testGetByUserAndConference() {
        when(applyDAO.findByUserAndConference(1, 1)).thenReturn(Flux.just(applicationModel, applicationModel2));

        StepVerifier.create(applyService.getByUserAndConference(1, 1))
                    .expectSubscription()
                    .expectNext(applicationModel)
                    .expectNext(applicationModel2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testDeleteComment() {
        when(applyCommentDAO.findById(1)).thenReturn(Mono.just(applicationCommentModel));
        when(applyCommentDAO.delete(applicationCommentModel)).thenReturn(Mono.empty().then());

        StepVerifier.create(applyService.deleteComment(1, 1))
                    .expectSubscription()
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testDeleteCommentBadId() {
        when(applyCommentDAO.findById(1)).thenReturn(Mono.just(applicationCommentModel));
        when(applyCommentDAO.delete(applicationCommentModel)).thenReturn(Mono.empty().then());
        applyService.deleteComment(1, 2);
        verify(applyCommentDAO, times(0)).delete(applicationCommentModel);
    }

    @Test
    void testFinishApplicationError() {
        when(reviewDAO.findByApply(1)).thenReturn(Flux.just(reviewModel, reviewModel2));

        StepVerifier.create(applyService.finishApplication(1))
                    .expectSubscription()
                    .expectErrorMessage("You must have 2 approvals")
                    .verify();
    }

    @Test
    void testGetApplicationsForCommitteeMemberError() {
        when(conferenceService.isUserRegisterToConferenceApp(1, 2)).thenReturn(Mono.just(false));

        StepVerifier.create(applyService.getApplicationsForCommitteeMember(1, 2))
                    .expectSubscription()
                    .expectErrorMessage("User is not part of the conference")
                    .verify();
    }

    @Test
    void testGetApplicationsForCommitteeMember() {
        when(conferenceService.isUserRegisterToConferenceApp(1, 2)).thenReturn(Mono.just(true));
        when(conferenceSectionDAO.findSectionsForCommitteeMember(2)).thenReturn(Flux.just(conferenceSection, conferenceSection2));
        when(conferenceSectionDAO.isSectionBelongsToConference(1, 1)).thenReturn(Mono.just(1));
        when(conferenceSectionDAO.isSectionBelongsToConference(1, 2)).thenReturn(Mono.just(1));
        when(applyDAO.findByConferenceSection(1, 1)).thenReturn(Flux.just(applicationModel));
        when(applyDAO.findByConferenceSection(1, 2)).thenReturn(Flux.just(applicationModel2));
        when(reviewDAO.findByApply(1)).thenReturn(Flux.just(reviewModel));
        when(reviewDAO.findByApply(2)).thenReturn(Flux.just(reviewModel2));

        StepVerifier.create(applyService.getApplicationsForCommitteeMember(1, 2))
                    .expectSubscription()
                    .expectNext(applicationModel)
                    .expectNext(applicationModel2)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testGetApplicationsForCommitteeMemberBadStatus() {
        applicationModel.setStatus(ApplyStatusEnum.DECLINED);
        when(conferenceService.isUserRegisterToConferenceApp(1, 2)).thenReturn(Mono.just(true));
        when(conferenceSectionDAO.findSectionsForCommitteeMember(2)).thenReturn(Flux.just(conferenceSection, conferenceSection2));
        when(conferenceSectionDAO.isSectionBelongsToConference(1, 1)).thenReturn(Mono.just(1));
        when(conferenceSectionDAO.isSectionBelongsToConference(1, 2)).thenReturn(Mono.just(1));
        when(applyDAO.findByConferenceSection(1, 1)).thenReturn(Flux.just(applicationModel));
        when(applyDAO.findByConferenceSection(1, 2)).thenReturn(Flux.just(applicationModel2));
        when(reviewDAO.findByApply(1)).thenReturn(Flux.just(reviewModel));
        when(reviewDAO.findByApply(2)).thenReturn(Flux.just(reviewModel2));

        StepVerifier.create(applyService.getApplicationsForCommitteeMember(1, 2))
                    .expectSubscription()
                    .expectNext(applicationModel2)
                    .expectComplete()
                    .verify();
    }
}