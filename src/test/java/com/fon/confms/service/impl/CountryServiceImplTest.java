package com.fon.confms.service.impl;

import com.fon.confms.dao.CountryDAO;
import com.fon.confms.model.CountryModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class CountryServiceImplTest {

    @InjectMocks
    private CountryServiceImpl countryService;

    @Mock
    private CountryDAO countryDAO;

    private CountryModel country1;
    private CountryModel country2;
    private CountryModel country3;

    @BeforeEach
    void setUp() {
        country1 = CountryModel.builder().id(1).name("Serbia").isoCode("SRB").build();
        country2 = CountryModel.builder().id(2).name("Spain").isoCode("ESP").build();
        country3 = CountryModel.builder().id(3).name("United Kingdom").isoCode("GBR").build();
    }

    @Test
    void testGetAll() {
        when(countryDAO.findAll()).thenReturn(Flux.just(country1, country2, country3));

        StepVerifier.create(countryService.getAll())
                    .expectSubscription()
                    .expectNext(country1)
                    .expectNext(country2)
                    .expectNext(country3)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();
    }

    @Test
    void testGetByID() {
        when(countryDAO.findById(1)).thenReturn(Mono.just(country1));

        StepVerifier.create(countryService.getByID(1))
                    .expectSubscription()
                    .expectNext(country1)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();
    }
}