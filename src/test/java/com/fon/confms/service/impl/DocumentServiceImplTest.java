package com.fon.confms.service.impl;

import com.fon.confms.dao.DocumentCategoryDAO;
import com.fon.confms.dao.DocumentDAO;
import com.fon.confms.model.DocumentCategoryModel;
import com.fon.confms.model.DocumentModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class DocumentServiceImplTest {

    @InjectMocks
    private DocumentServiceImpl documentService;

    @Mock
    private DocumentDAO documentDAO;

    @Mock
    private DocumentCategoryDAO documentCategoryDAO;

    private DocumentModel documentModel;
    private DocumentModel documentModel2;
    private DocumentCategoryModel documentCategoryModel;
    private DocumentCategoryModel documentCategoryModel2;

    @BeforeEach
    void setUp() {
        byte[] file = new byte[10];

        documentModel = DocumentModel.builder()
                                     .id(10)
                                     .name("Test document")
                                     .description("Description")
                                     .fileName("Test file")
                                     .file(file)
                                     .build();

        documentModel2 = DocumentModel.builder()
                                      .id(10)
                                      .name("Test document2")
                                      .description("Description2")
                                      .fileName("Test file2")
                                      .file(file)
                                      .build();

        documentCategoryModel = DocumentCategoryModel.builder().id(1).name("Scientific work").build();
        documentCategoryModel2 = DocumentCategoryModel.builder().id(2).name("Master thesis").build();
    }

    @Test
    void testGetAll() {
        when(documentDAO.findAll()).thenReturn(Flux.just(documentModel, documentModel2));
        StepVerifier.create(documentService.getAll())
                    .expectSubscription()
                    .expectNext(documentModel)
                    .assertNext(document -> assertThat(document.getName()).isEqualTo("Test document2"))
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetByID() {
        when(documentDAO.findById(1)).thenReturn(Mono.just(documentModel));
        StepVerifier.create(documentService.getByID(1))
                    .expectSubscription()
                    .assertNext(document -> assertThat(document.getName()).isEqualTo("Test document"))
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetAllDocumentCategories() {
        when(documentCategoryDAO.findAll()).thenReturn(Flux.just(documentCategoryModel, documentCategoryModel2));
        StepVerifier.create(documentService.getCategories())
                    .expectSubscription()
                    .expectNext(documentCategoryModel)
                    .assertNext(document -> assertThat(document.getName()).isEqualTo("Master thesis"))
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetCategory() {
        when(documentCategoryDAO.findById(1)).thenReturn(Mono.just(documentCategoryModel));
        StepVerifier.create(documentService.getCategory(1))
                    .expectSubscription()
                    .expectNext(documentCategoryModel)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testCreateDocument() {
        when(documentDAO.save(any())).thenReturn(Mono.just(documentModel));
        StepVerifier.create(documentService.createDocument(documentModel))
                    .expectSubscription()
                    .expectNext(documentModel)
                    .expectComplete()
                    .verify();
    }
}