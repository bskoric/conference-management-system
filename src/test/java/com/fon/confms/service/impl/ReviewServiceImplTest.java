package com.fon.confms.service.impl;

import com.fon.confms.dao.ApplyDAO;
import com.fon.confms.dao.ReviewDAO;
import com.fon.confms.model.ApplyModel;
import com.fon.confms.model.ReviewModel;
import com.fon.confms.model.UserModel;
import com.fon.confms.model.enums.ApplyStatusEnum;
import com.fon.confms.model.enums.ReviewStatusEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class ReviewServiceImplTest {

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Mock
    private ReviewDAO reviewDAO;

    @Mock
    private ApplyDAO applyDAO;

    private ReviewModel reviewModel;
    private ReviewModel reviewModel2;
    private UserModel committeeUserModel;
    private ApplyModel applicationModel;

    @BeforeEach
    void setUp() {
        UserModel userModel = UserModel.builder().id(1).username("userUsername").build();
        committeeUserModel = UserModel.builder().id(1).username("cmUsername").build();
        UserModel committeeUserModel2 = UserModel.builder().id(2).username("cmUsername2").build();
        applicationModel = ApplyModel.builder().id(1).conferenceMember(userModel).status(ApplyStatusEnum.IN_REVIEW).build();
        reviewModel = ReviewModel.builder()
                                 .id(1)
                                 .apply(applicationModel)
                                 .committeeMember(committeeUserModel)
                                 .status(ReviewStatusEnum.APPROVED)
                                 .build();
        reviewModel2 = ReviewModel.builder()
                                  .id(2)
                                  .apply(applicationModel)
                                  .committeeMember(committeeUserModel2)
                                  .status(ReviewStatusEnum.IN_PROGRESS)
                                  .build();
    }

    @Test
    void testGetById() {
        when(reviewDAO.findById(1)).thenReturn(Mono.just(reviewModel));
        StepVerifier.create(reviewService.getByID(1))
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetAll() {
        when(reviewDAO.findAll()).thenReturn(Flux.just(reviewModel, reviewModel2));
        StepVerifier.create(reviewService.getAll())
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectNext(reviewModel2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }


    @Test
    void testGetReview() {
        when(reviewDAO.findByCommitteeMemberIdAndApply(1, 1)).thenReturn(Mono.just(reviewModel));
        StepVerifier.create(reviewService.getReview(1, 1))
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetReviews() {
        when(reviewDAO.findByConferenceMember(1, 1)).thenReturn(Flux.just(reviewModel));
        StepVerifier.create(reviewService.getReviews(1, 1))
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetReviews2() {
        when(reviewDAO.findByApply(1)).thenReturn(Flux.just(reviewModel, reviewModel2));
        StepVerifier.create(reviewService.getReviews(1))
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectNext(reviewModel2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testCreateReview() {
        when(reviewDAO.save(any())).thenReturn(Mono.just(reviewModel));
        when(applyDAO.updateStatus(ApplyStatusEnum.IN_REVIEW.toString(), 1)).thenReturn(Mono.empty().then());

        StepVerifier.create(reviewService.createReview(applicationModel, committeeUserModel))
                    .expectSubscription()
                    .expectNext(reviewModel)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testCreateReviewError() {
        when(reviewDAO.save(any())).thenReturn(Mono.error(new DataIntegrityViolationException("User is already reviewer")));

        StepVerifier.create(reviewService.createReview(applicationModel, committeeUserModel))
                    .expectSubscription()
                    .expectErrorMessage("User is already reviewer")
                    .verify();

    }

    @Test
    void testChangeStatus() {
        reviewService.changeStatus(1, ReviewStatusEnum.APPROVED);
        verify(reviewDAO, times(1)).updateStatus(ReviewStatusEnum.APPROVED.toString(), 1);
    }
}