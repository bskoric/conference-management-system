package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceCategoryDAO;
import com.fon.confms.model.ConferenceCategoryModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class ConferenceCategoryServiceImplTest {

    @InjectMocks
    private ConferenceCategoryServiceImpl conferenceCategoryService;

    @Mock
    private ConferenceCategoryDAO conferenceCategoryDAO;

    private ConferenceCategoryModel conferenceCategoryModel;
    private ConferenceCategoryModel conferenceCategoryModel2;
    private ConferenceCategoryModel conferenceCategoryModel3;

    @BeforeEach
    void setUp() {
        conferenceCategoryModel = ConferenceCategoryModel.builder().id(1).name("IT").description("Description").build();
        conferenceCategoryModel2 = ConferenceCategoryModel.builder().id(2).name("Database").description("Description").build();
        conferenceCategoryModel3 = ConferenceCategoryModel.builder().id(2).name("AI").description("Description").build();
    }

    @Test
    void testGetAll() {
        when(conferenceCategoryDAO.findAll()).thenReturn(Flux.just(conferenceCategoryModel,
                                                                   conferenceCategoryModel2,
                                                                   conferenceCategoryModel3));
        StepVerifier.create(conferenceCategoryService.getAll())
                    .expectSubscription()
                    .expectNext(conferenceCategoryModel)
                    .expectNext(conferenceCategoryModel2)
                    .expectNext(conferenceCategoryModel3)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetByID() {
        when(conferenceCategoryDAO.findById(1)).thenReturn(Mono.just(conferenceCategoryModel));
        StepVerifier.create(conferenceCategoryService.getByID(1))
                    .expectSubscription()
                    .expectNext(conferenceCategoryModel)
                    .expectComplete()
                    .verify();

    }
}