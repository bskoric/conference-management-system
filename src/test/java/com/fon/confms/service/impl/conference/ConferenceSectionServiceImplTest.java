package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceSectionDAO;
import com.fon.confms.model.ConferenceCategoryModel;
import com.fon.confms.model.ConferenceSectionModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class ConferenceSectionServiceImplTest {

    @InjectMocks
    private ConferenceSectionServiceImpl conferenceSectionService;

    @Mock
    private ConferenceSectionDAO conferenceSectionDAO;

    private ConferenceSectionModel conferenceSection;
    private ConferenceSectionModel conferenceSection2;

    @BeforeEach
    void setUp() {
        ConferenceCategoryModel
            conferenceCategory = ConferenceCategoryModel.builder().id(1).name("IT").description("Description").build();
        conferenceSection = ConferenceSectionModel.builder().id(1).name("Database section").category(conferenceCategory).build();
        conferenceSection2 = ConferenceSectionModel.builder().id(2).name("Software development").category(conferenceCategory).build();
    }

    @Test
    void testGetById() {
        when(conferenceSectionDAO.findById(1)).thenReturn(Mono.just(conferenceSection));
        StepVerifier.create(conferenceSectionService.getByID(1))
                    .expectSubscription()
                    .expectNext(conferenceSection)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetAll() {
        when(conferenceSectionDAO.findAll()).thenReturn(Flux.just(conferenceSection, conferenceSection2));
        StepVerifier.create(conferenceSectionService.getAll())
                    .expectSubscription()
                    .expectNext(conferenceSection)
                    .expectNext(conferenceSection2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetSectionsForConference() {
        when(conferenceSectionDAO.findSectionsForConference(1)).thenReturn(Flux.just(conferenceSection, conferenceSection2));
        StepVerifier.create(conferenceSectionService.getSectionsForConference(1))
                    .expectSubscription()
                    .expectNext(conferenceSection)
                    .expectNext(conferenceSection2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetSectionsForCategory() {
        when(conferenceSectionDAO.findSectionsForCategory(1)).thenReturn(Flux.just(conferenceSection, conferenceSection2));
        StepVerifier.create(conferenceSectionService.getSectionsForCategory(1))
                    .expectSubscription()
                    .expectNext(conferenceSection)
                    .expectNext(conferenceSection2)
                    .expectNextCount(0)
                    .expectComplete()
                    .verify();
    }
}