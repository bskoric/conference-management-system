package com.fon.confms.service.impl.conference;

import com.fon.confms.dao.ConferenceDAO;
import com.fon.confms.dao.UserDAO;
import com.fon.confms.model.ConferenceModel;
import com.fon.confms.model.enums.UserStatusEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@SpringBootTest()
@ExtendWith(MockitoExtension.class)
class ConferenceServiceImplTest {

    @InjectMocks
    private ConferenceServiceImpl conferenceService;

    @Mock
    private ConferenceDAO conferenceDAO;

    private ConferenceModel conferenceModel;
    private ConferenceModel conferenceModel2;

    @BeforeEach
    void setUp() {
        conferenceModel = ConferenceModel.builder().id(1).name("Test conference 1").town("Belgrade").isClosed(false).build();
        conferenceModel2 = ConferenceModel.builder().id(2).name("Test conference 2").town("Belgrade").isClosed(false).build();
    }

    @Test
    void testGetAll() {
        when(conferenceDAO.findAll()).thenReturn(Flux.just(conferenceModel, conferenceModel2));
        StepVerifier.create(conferenceService.getAll())
                    .expectSubscription()
                    .expectNext(conferenceModel)
                    .expectNext(conferenceModel2)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testGetByID() {
        when(conferenceDAO.findById(1)).thenReturn(Mono.just(conferenceModel));
        StepVerifier.create(conferenceService.getByID(1))
                    .expectSubscription()
                    .expectNext(conferenceModel)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testIsUserRegisterToConferenceApp() {
        when(conferenceDAO.isUserRegisterToConferenceApp(1, 1)).thenReturn(Mono.just(1));

        StepVerifier.create(conferenceService.isUserRegisterToConferenceApp(1, 1))
                    .expectSubscription()
                    .expectNext(true)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testIsUserRegisterToConferenceAppFalse() {
        when(conferenceDAO.isUserRegisterToConferenceApp(1, 1)).thenReturn(Mono.just(0));

        StepVerifier.create(conferenceService.isUserRegisterToConferenceApp(1, 1))
                    .expectSubscription()
                    .expectNext(false)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testRegisterToConferenceApp() {
        when(conferenceDAO.registerToConference(1, 1, UserStatusEnum.PENDING_APP_REGISTRATION.toString())).thenReturn(Mono.just(1));

        StepVerifier.create(conferenceService.registerToConferenceApp(1, 1))
                    .expectSubscription()
                    .expectNext(true)
                    .expectComplete()
                    .verify();

    }

    @Test
    void testRegisterToConferenceAppFalse() {
        when(conferenceDAO.registerToConference(1, 1, UserStatusEnum.PENDING_APP_REGISTRATION.toString())).thenReturn(Mono.just(0));

        StepVerifier.create(conferenceService.registerToConferenceApp(1, 1))
                    .expectSubscription()
                    .expectNext(false)
                    .expectComplete()
                    .verify();

    }
}